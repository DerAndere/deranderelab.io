---
layout: post
title: A supreme strategy game - Supreme Commander with Forged Alliance Forever (FAF) mod
date:   2020-08-02 00:00:00 -0300
author: DerAndere
categories: tech
permalink: /supreme-commander/
tags: [tech, open-source, software-tips]
---

**[Werbung ohne Auftrag / unpaid advertisement]**

The computer game *Supreme Commander* by Gas Powered Games set a milestone in the real time strategy (RTS) genre, especially since it was extended with the release of *Supreme Commander: Forged Alliance* (available as a bundled stand-alone game). The game can be purchased via Valve's platform Steam. In case you have bought a physical copy of either the base game or *Supreme Commander: Forged Alliance* instead, you can use the serial key from the box to also activate a license for the game inside Steam. After you have run the game once and created a profile, you can exit the game and install *Downlord's FAF lobby client* (open source) which is provided by the *Forged Alliance Forever* (FAF) modding community at [https://www.faforever.com/](https://www.faforever.com/) (source code at [https://github.com/FAForever/downlords-faf-client](https://github.com/FAForever/downlords-faf-client)). To set it up, follow the instructions at [https://wiki.faforever.com/index.php?title=Setting_Up_FAF](https://wiki.faforever.com/index.php?title=Setting_Up_FAF) and [https://wiki.faforever.com/index.php?title=How_do_i_play_FAF_offline%3F](https://wiki.faforever.com/index.php?title=How_do_i_play_FAF_offline%3F):

1. Install the game *Supreme Commander: Forged Alliance*
1. Install the *Steam client* by Valve
1. Register a user account at Steam and log in to Steam via the Steam client
1. If you have bought *Supreme Commander* and/or *Supreme Commander: Forged Alliance* physical copy, you can use the serial key to activate a license for *Supreme Commander: Forged Alliance* inside Steam.
1. Download and install *Downlord's FAF lobby client* from [https://www.faforever.com/](https://www.faforever.com/)
1. Run the Steam client, select *Supreme Commander: Forged Alliance* from your game library inside Steam and select "Play". Accept all conditions (you may have to get permission by the administrator and if asked to allow access, the change of firewall settings may have to be accepted by the administarator).
1. In *Supreme Commander: Forged Alliance*, create a player profile, then exit *Supreme Commander: Forged Alliance*. 
1. Double-click on  "Downlord's FAF Client" (the shortcut to downlords-faf-client.exe) to run the lobby client, register an FAF account in the lobby client or log in to an existing FAF account.
1. Inside the lobby client, select "Play" -> "Custom Game" -> "Create"
1. FAF might ask you to specify the Forged Alliance folder so just simply direct it to your *Supreme Commander: Forged Alliance* folder on your hard drive. If you installed *Supreme Commander: Forged Alliance* on Steam, the game is located in your Steam Library folder. To find that folder, start Steam, go to "Steam - Settings" -> "Downloads" -> "STEAM LIBRARY FOLDERS". The Steam Library folder has a folder steamapps\common, there you should find your Supreme Commander Forged Alliance folder.
1. launch a password-protected single player game without observers (can be made visible only for friends).
1. Open the in-game main menu and select "Exit to Windows" to close *Supreme Commander: Forged Alliance*.
1. Close *Downlord's FAF lobby client*
1. Create a shortcut to `C:/ProgramData/FAForever/bin/ForgedAlliance.exe` and move the shortcut to a convenient place. 
1. Right-click on the shortcut to `C:/ProgramData/FAForever/bin/ForgedAlliance.exe` and from the context-menu, select "Properties" -> "Shortcut" -> "Advanced". Left-click on the checkbox to enable "Run as administrator", then left-click "OK" -> "Apply" -> "OK" . 
1. Double-clicking on the modified shortcut to `C:/ProgramData/FAForever/bin/ForgedAlliance.exe` to run ForgedAlliance.exe with administrator privileges. This starts the game with the Forged Alliance Forever mod and other built-in mods (Hotbuild mod and Unit Manager 2.0 and Sorian Artificial Intelligence are built-in).
1. Left-Click Options -> Gameplay, set `Accept Build Templates = on`
1. Left-Click Options -> Interface, set `Hotkey Labels = on`
1. Select "Skirmish". Select a map, fill other player slots with Sorian artificial intelligence or close player slots as desired. Set the desired victory conditions, enable additional installed mods, select "Options" -> "Restrictions" and set unit restrictions, then left-click "Launch Game" . 
1. While the match is running, open the in-game main menu and left-click "Keybindings" (Hotkey: F1) -> "Hotbuild preset" .

If you want to easily install additional mods or maps, quit ForgedAlliance.exe and do the following: 

1. Run downlords-faf-client.exe . 
1. Log in to your FAF account (or register a new FAF account). 
1. Click "Maps" or "Mods" to install new community maps or mods.

<br/>

Copyright 2020 - 2021 DerAndere
