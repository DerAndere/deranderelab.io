---
layout: post
title:  "Engineerig tools for optimizing a simulated system model based on data"
date:   2020-05-15 00:00:00 -0300
author: DerAndere
categories: data-analysis
permalink: /engineering-and-modeling-tools
tags: [data-analysis, programming, Java, Python, data]
---

The [Bio7 / Eclipse IDE](https://bio7.org/) can be extended using the plug-in extensions ["EMF"](https://download.eclipse.org/modeling/emf/emf/builds/index.html), "g9 Database import", "EMF compare" to create models (virtual representations or realworld or abstract systems) using graphical programming with the Eclipse Modeling Framework (EMF). The EMF extension auto-generates Java code which can quickly be run from the Eclipse toolbar. A new Eclipse-based Java application is started which provides a graphical user interface (GUI) to input data and analyze the outcome of the modeled system. This makes it easy to simulate how the input (changes to model parameters) is processed by the modeled system and how that affects certain other parameters of the modeled system. The Eclipse plug-in "EcoreTools" provides a graphical editor for visual programming of EMF models using UML-style block diagrams. The [Modelica Modeling Language plugin (ModelicaML)](https://www.openmodelica.org/?id=139:modelicaml) provides a domain-specific UML profile (more specifically, a SysML dialect) that allows visual programming of models and automatic generation of Modelica code, allowing simulation and analysis of physical systems. The current implementation of ModelicaML uses the SysML component of the Eclipse plugin Papyrus and the code generator Acceleo. An alternative is to use OMEdit which is part of the [OpenModelica IDE](https://www.openmodelica.org/download/download-windows#) for visual programming and export of Modelica code and further programming Modelica models directly using the OpenModelica Eclipse plugin "Modelica Development Tooling" (MDT). It was suggested to [replace the current implementation of ModelicaML by a small EMF-based core](https://ep.liu.se/en/conference-article.aspx?series=ecp&issue=29&Article_No=3). The Eclipse plug-in ["EMF Forms"](https://www.eclipse.org/ecp/emfforms/download.html) adds the ability to create forms as user interfaces for a model.

<br/>

Copyright 2020 - 2021 DerAndere
