---
layout: post
title:  "Computer history"
date:   2018-01-08 00:00:00 -0300
author: DerAndere
categories: tech
permalink: /computer-history
tags: [tech, history] 
---

Here you can find literature on the history of early computers and
theoretic background on how they work.

## A brief history of computers

At all times have humans created tools and machines that were designed
to make life easier. The next step was to create machines that could
react to input by changing state, thereby being able to accommodate for
different situations and tasks. What dictates the nature of the
machine’s state based on the the input can be called a program. First
programs were completely implemented in hardware, as illustrated by
ancient programmable analog "computers" such as
[Ismail
al-Jazari](https://en.wikipedia.org/wiki/Ismail_al-Jazari)’s
astronomical
[castleclock](http://muslimheritage.com/article/al-jazari%E2%80%99s-castle-water-clock-analysis-its-components-and-functioning)
which featured a reprogrammable length of the day (1206 AD). The extent
of programmability extended Building on calculating machines by [Wilhelm
Schickard](https://en.wikipedia.org/wiki/Wilhelm_Schickard) (1623) and
Blaise Pascal (developed between 1642 and 1649), [Charles
Babbage](https://en.wikipedia.org/wiki/Charles_Babbage) and his son
Henry conceptualized the first universally programmable (Turing-complete
if it had infinite storage capacity) mechanical computer, the Analytical
Engine (1888-1906). They planned to use punched cards for storage.
Punched paper tapes had been invented by Basile Bouchon in 1725 to
control the patterns woven by a loom and in 1804/1805 Joseph Marie
Jacquard had demonstrated a mechanism that used punch cards to fully
automate loom operation. In 1835, Semen Korsakov presented devices for
the mechanical readout and comparison of information stored in punched
cards to the Imperial Academy of Sciences in St. Petersburg.

In 1906, the automatic [Feed Tabulator type 1 of
the Electric Tabulating
System](http://www.columbia.edu/cu/computinghistory/tabulator.html)
by Herman Hollerith (produced by The Tabulating Company) was extended by
a control board   allowing functions to
be encoded using jumper wires for that year's U.S. census. After Herman
Hollerith sold his company, the resulting company was renamed to
International Business Machines Corporation (IBM) and [his machine was
sold under the name IBM
type 090](http://www.columbia.edu/cu/computinghistory/tabulator.html).
[Further contributions came from L.
Cobrie](https://doi.org/10.1093/mnras/88.5.447) and [Wallace J. Eckert
who pioneered complex calculation on calculating
machines](http://www.columbia.edu/cu/computinghistory/eckert.html). The
Z3 by Conrad Zuse from 1941 was the first universally programmable
electro-mechanic computer whose switches were mechanical relays, [but it
lacked the conditional branching of a Turing
machine](https://en.wikipedia.org/wiki/Manchester_Baby). To increase
speed and prevent wear-out, vacuum tubes were introduced as electric
switches in later computers. The first programmable electric digital
computer was the Colossus built by Flowers in 1943. The first
Turing-complete (if it had infinite storage capacity) computer of this
type was the [Electronic Numerical Integrator
and Computer (ENIAC)](https://de.wikipedia.org/wiki/ENIAC) that
was completed in 1946. The first Stored-Program Computers (i.e.
controllable by a program stored in a main random access memory (RAM)
were IBM's Selective Sequence Electronic Calculator (SSEC), built under
the direction of Columbia Professor Wallace Eckert and his staff in
1946-47 and the [Small-Scale Experimental Machine (SSEM) or "Manchester
Baby" from 1948](https://en.wikipedia.org/wiki/Manchester_Baby). The
first fully capable computer that implemented the [von Neumann
Architecture envisioned in 1045](http://dx.doi.org/10.1109/85.238389)
(see section 2.2) was the [Electronic Delay Storage Automatic Calculator
(EDSAC) by Maurice V. Wilkes and his team
from 1949](https://de.wikipedia.org/wiki/Electronic_Delay_Storage_Automatic_Calculator).
[The Ferranti Mark 1 or "Machester Electronic Computer" was the first
turing complete general purpose computer to be commercially
available](https://en.wikipedia.org/wiki/Ferranti_Mark_1). Arguably the
first mini computer was the
[PDP-1](https://de.wikipedia.org/wiki/Programmed_Data_Processor) by
Digital Equipment Solution (DEC) from 1960. A more user-friendly mini
computer called [LINC](https://en.wikipedia.org/wiki/LINC) was designed
by Wesley A. Clark, Charles Molnar and others at MIT in 1962 and its
commercial production by DEC started in 1964 as a precessor of the
[PDP-8](https://en.wikipedia.org/wiki/Digital_Equipment_Corporation).

## Semiconductors, doping and transistors

The next revolution in computing was the advent of transistors and
further miniaturization. Therefore, I would like to point you to
information on how transistors work. As I come from a chemistry
background, I was baffled how different the terms used by physicists and
chemists are when they talk about solid state matter. However, it was
enlightening to see that the [underlying concepts of band theory and
ligand field theory / molecular orbital
theory](https://onlinelibrary.wiley.com/doi/pdf/10.1002/anie.198708461)
(see [here](https://www.ch.ic.ac.uk/harrison/Group/Mallia.tmp/3year_lab_elec_str/hoffmann.pdf))
are [basically the same](https://is.muni.cz/el/1431/podzim2006/C7780/um/LN/L4_band_theory.pdf).

In order to understand how semiconductor-based circuit components work,
it is advisable to have a look at how doping affects the properties of
materials. Recent studies reveal the electronic structure of doped
materials. (see
[here](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.71.245308),
[here](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.79.033204),
[here](https://journals.aps.org/pra/abstract/10.1103/PhysRevA.79.053201)
and [here](https://doi.org/10.1364/OL.34.001549))

Doping of semiconductors affects their band gap and thereby their
conductivity. The electronic properties at the junction between regions
of the semiconductor that are negatively doped (impurities with one
valence electron less than the bulk material) and positively doped
(impurities with one valence electron in excess) allow for transistors,
diodes and other electronic circuit compounds to be produced from
semiconductors. 

The rectifying effect was first described in
[Braun, F. (1874), Über die Stromleitung durch
Schwefelmetalle \[On current conduction through metal sulfides\],
*Annalen der Physik und Chemie*, **153** (4):
556-563.](http://onlinelibrary.wiley.com/doi/10.1002/andp.18752291207/abstract;jsessionid=36CAC530DA40E11C931FE986D2D252EE.f04t02)
and an early description of the effect of the surface on the current of
a semiconductor device was made by  [Adams, W.G.
and Day, R.E. (1877).
The Action of Light on Selenium, *Phil. Trans.
R. Soc. Lond*. **167**, 313-349](http://rstl.royalsocietypublishing.org/content/167/313.extract).
The first description of an unheated device with properties similar to a
vacuum tube was patented in 1925 by Julius Lilienfeld (Patents [US1745175](http://worldwide.espacenet.com/publicationDetails/biblio?locale=de_EP&CC=US&NR=1745175), [US1900018](https://www.google.com/patents/US1900018))
and later Oskar Heil patented his field effect transistor in 1934
([Patent GB439457](http://worldwide.espacenet.com/publicationDetails/biblio?locale=de_EP&CC=GB&NR=439457)).
But the technology for realizing these constructions were not available
at that time.

Theoretic work on surface states by Igor Tamm and William Shockley
(Tamm, I. (1932) On the possible bound states of electrons on a crystal
surface. *Phys. Z. Soviet Union*. **1**: 733;
[Shockley 1939](https://doi.org/10.1103/PhysRev.56.317)) as well as the
theories on rectification by Mott and extensions by Davydov and by
Shottky at Siemens AG explained, that at point contacts on charged
semiconductors, extra electrons from the charged bulk material would
flow into the point contact and form a neutral barrier layer. The
Mott-Shottky-Davydov theory was published in 1939 and the more accurate
[theory by Bethe](http://www.worldcat.org/title/theory-of-the-boundary-layer-of-crystal-rectifiers/oclc/13618604)
in 1942. Herbert F. Mataré and Heinrich Welker are said to have observed
the transistor effect while experimenting with duodiodes between 1942
and 1944. [At Compagnie des Freins et Signaux
Westinghouse](https://spectrum.ieee.org/tech-history/silicon-revolution/how-europe-missed-the-transistor),
Heinrich Welker envisaged his concept of the field effect transistor.
[Materé and Welker soon became aware of the
limited modulation with their first devices in 1945 and
continued](http://darwin.bth.rwth-aachen.de/opus3/volltexte/2008/2517/pdf/Handel_Kai.pdf)
development [for the next years](https://spectrum.ieee.org/tech-history/silicon-revolution/how-europe-missed-the-transistor).
Similarly, [theoretic work](https://doi.org/10.1103/PhysRev.56.317)
[on the field effect by William Shockley and
G.L. Pearson from 1945 and initial experiments with semiconductor device
similar too a MOSFET](https://doi.org/10.1103/PhysRev.74.232)
showed less modulation than expected. [
Shockleys new empoyee John Bardeen came up with the explanation that
electrons could become trapped in surface state not only at point
contacts but at every semiconductor surface.](http://darwin.bth.rwth-aachen.de/opus3/volltexte/2008/2517/pdf/Handel_Kai.pdf)
These trapped electrons [cannot contribute to
conductivity and shield the bulk semiconductor from external
influence](http://www.springer.com/de/book/9783540342571). The
result enabled [Bardeen and Walter Brattain to
manufacture the first point contact transistor at Shockleys lab in
1947](https://doi.org/10.1103/PhysRev.74.230). The independently
developed European field effect transistor of Herbert F. Mataré and
Walker was presented  shortly after in 1948 (patents
[FR1010427](http://worldwide.espacenet.com/publicationDetails/biblio?locale=de_EP&CC=FR&NR=1010427),
[US2673948](http://worldwide.espacenet.com/publicationDetails/biblio?locale=de_EP&CC=US&NR=2673948)).
In the following years, different circuit components were manufactured
by placing differently doped semiconductor regions next to each other.

![Fig. 1](/assets/transistor.jpg)

Fig. 1: Replication of the point contact transistor that was internally
presented at Bell Labs in December 1947. (Source:
[http://clinton4.nara.gov/media/jpg/replica-of-first-transistor.jpg,](http://clinton4.nara.gov/media/jpg/replica-of-first-transistor.jpg)
assumed to be public domain

The [first silicon-basedtransistor was
manufactured by Morris Tanenbaum at Bell Labs in
1954](https://dx.doi.org/10.1063%2F1.1722071). In the same year,
Gordon Kidd Teal at Texas Instruments presented his silicon-based
Transistor which was commercialized in 1955. After  Tanenbaum,
  [Calvin S.
Fuller](https://de.wikipedia.org/wiki/Calvin_Souther_Fuller) and others
had developed the gas diffusion process for efficient doping in
1954/1955 Silicon-based transistor production.

Shortly after, the first computers with all vacuum tubes being replaced
by discrete transistors emerged: The [prototype IBM 604 transistorized
calculator (presented in 1954)](https://en.wikipedia.org/wiki/IBM_604)
and the [Harwell CADET](https://en.wikipedia.org/wiki/Harwell_CADET),
which first operated in February 1955. [The IBM 608 transistor
calculator was the first commercially available fully transistorized
computer. It was announced in April 1955 and first shipped in
December 1957](http://www-03.ibm.com/ibm/history/exhibits/vintage/vintage_4506VV2214.html).
After [the surface-barrier transistor was developed by Philco in 1953,
the Philco Transac models S-1000 scientific computer and S-2000
electronic data processing computer were announced in 1957 but did not
ship until sometime after the fall
of 1958](https://en.wikipedia.org/wiki/Transistor_computer).

All the semiconductor-based cirquit components were screaming for
miniaturization and after initial trials of by Werner Jacobi (Siemens
AG) and G.W.A. Dummer that were aimed at integrating several circuit
components, an operational Germanium-based integrated circuit (IC) was
realized by Jack Kilby at Texas Instruments in 1958. After [Kurt
Lehovec](https://en.wikipedia.org/wiki/Kurt_Lehovec) of Sprague Electric
had discovered the principle of [p–n junction
isolation](https://en.wikipedia.org/wiki/P%E2%80%93n_junction_isolation)
(Patent [US3029366
A](https://www.google.com/patents/US3029366)), a group at
Fairchild Semiconductor led by Jay Last was the first to produce ICs
reliably on a single silicon substrate based on the planar process by
Jean Hoemi (Patente [US
3025589](https://worldwide.espacenet.com/textdoc?DB=EPODOC&IDX=US3025589),
[US
3064167](https://worldwide.espacenet.com/textdoc?DB=EPODOC&IDX=US3064167))
and Robert Noyce’s developments. [Fairchild Semiconductor created the
first ALU integrated circuit, the FSC4711 in 1968 but only few of these
chips could be produced. They then developed the FSC9340 and the FSC9341
which did not include a carry look-ahead function. Since patenting chip
designs was not possible at that time, Texas Instruments and Fairchild
copied each other designs and because Texas Instruments had bigger
market share, they adopted the numbering scheme of the TI 54/74 series.
The FSC9341 was since then better known as the 74181 chip (e.g. Texas
Instruments SN74181)](https://apollo181.wixsite.com/apollo181/about).
[In 1971 the first single-chip microprocessors and microcontrollers were
developed:](https://en.wikipedia.org/wiki/Microprocessor) Starting with
limited functionality calculator-on-a-chip designs like the [Mostek
MK6010](http://www.vintagecalculators.com/html/the_calculator-on-a-chip.html)
and the chip
[GI 250](http://www.vintagecalculators.com/html/the_calculator-on-a-chip.html)
of the [Monroe/Litton Royal Digital III calculator by Pico Electronics
and General Instrument](https://en.wikipedia.org/wiki/Microprocessor) as
well as the TMS0100 calculator-on-a-chip family including the TMS1802NC
by Texas Instruments created by Gary Boone and Michael Cochran at Texas
Instruments, chips became more and more tailored towards general purpose
computing, resulting in the TMS 1000 (patents
[US3757306](https://patents.google.com/patent/US3757306) and
[US4074351](https://patents.google.com/patent/US4074351)), which went on
the market in 1974. [The Intel 4004 chip was the first commercially
available single-chip CPU, announced in November 1971 as Intel
C4004](https://en.wikipedia.org/wiki/Microprocessor).

## Graphical user interfaces

This development coincided with the advent of personal computers. Modern
human – machine interaction was pioneered at the Stanford Research
Institute of the Stanford University by Dough Engelbart ([inventor of
the computer
mouse](http://www.computinghistory.org.uk/det/2426/A-Research-Center-for-Augmenting-Human-Intellect/)),
Dustin Lindberg, Bill English and others. They demonstrated their
oN-Line System (NLS) featuring editing of texts and graphs,
collaborative work, hyperlinks, computer-mouse control and a GUI with
program windows on a Scientific Data Systems SDS 940 in 1968 ([watch
"the mother of all demos" or "The Demo"
online](https://web.stanford.edu/dept/SUL/library/extra4/sloan/MouseSite/1968Demo.html#complete)).
David A. Evans developed the Journal feature during his phD studies.

After Scientific Data Systems was acquired by Xerox in 1969 and became
Xerox Data Systems, several coworkers of the Stanford Research Institute
were recruited by Xerox to develop commercially available computers
inspired by the NLS. At the same time, the [Datapoint 2200 was designed
by Computer Terminal Corporation (CTC) founders Phil Ray and Gus Roche,
announced by CTC in 1970 and shipped in 1971. It featured a keyboard as
well as a computer screen and its CPU Architecture and instruction set
became the basis of the x86 architecture of the Intel 8086 (which has
limited source-code compatiblity to Intel 8080 which in turn is source
code-compatible to the Intel 8008 from 1972, the first 8 bit single chip
CPU)](https://de.wikipedia.org/wiki/Datapoint_2200). In 1973, the "Sac
State 8008" system was operational at California State University,
Sacramento. Bill Pentz, who led a team on the COmputerized MEdical
REcords System (COMERs) at the Computer Science Department within the
School of Engineering at California State University, Sacramento, was
developing a simple general purpose computer based on the Intel 8008.
When asking a friend who worked at Tektronix Inc. for advice, he was
offered a position at that company and Tektronix Inc. built him a
reliable custom computer based on the Intel 8008. This computer was
fitted with a with a Tektronix 4023 graphical terminal and a memory
ROM/RAM expansion card. Bill Pentz developed firmware to run IBM’s Basic
Assembler Language (BAL) on the Intel 8008. Using code for an assembler,
a BASIC interpreter and an operating system that every student had to
submit for examinations, Bill Pentz made the system rund a Disk
Operating System. A Tektronix 9600 baud serial communications interface
from 1973 was also also added. [The printed circuid boards and original
manuals of components were preserved by John Moorhead and are on display
at the Digibarn
Museum](http://www.digibarn.com/stories/bill-pentz-story/08-09-27-BillPenzVisit/IMG_2039.jpg).

![Fig. 2](/assets/Sac_State_8008_System.jpg)

Fig. 2: CAD drawing of the "Sac State 8008" system. This image is
Copyright 1998 - 2018 [Digibarn Computer
Museum](http://www.digibarn.com), some rights reserved under the
[Creative Commons Attribution-Noncommercial 3.0
License](http://creativecommons.org/licenses/by-nc/3.0/).

In [1973 the minicomputer Xerox Alto was introduced at the Xerox Palo
Alto Research Center (Xerox PARC). It was designed mostly by Charles P.
Thacker. Industrial Design and manufacturing was sub-contracted to
Xerox. It was used internally and at universities and featured
everything above as well as a GUI using the desktop metaphor first
introduced by Alan Kay at Xerox PARC
in 1970](https://en.wikipedia.org/wiki/Xerox_Alto). It was the basis of
the first commercially available personal computer featuring all this,
the [Xerox 8081 Information System ("Xerox Star")
of 1981](https://en.wikipedia.org/wiki/Xerox_Star).

The first commercially available computer based on a single-chip
microprocessor that also featured a keyboard and a monitor (CRT display)
was the Apple I from 1976 that was designed by Steve Wozniak. It was
programmed using Apple BASIC but featured no real operating system.
[After the Apple II from 1977 originally was fitted with a 5¼" floppy
disk system in 1978, a Disk Operating System was commissioned from the
company Shepardson Microsystems where its development was done by Paul
Laughton](https://en.wikipedia.org/wiki/Apple_II_series). [After Intel
had released the 16 Bit CPU Intel 8086 and Tim Peterson had developed
the operating system QDOS for it, Microsoft licenced QDOS-86 and ported
it for the IBM Personal Computer (PC) that was released with a debugged
version of MS-DOS 1.14, called
PC-DOS 1.0](https://de.wikipedia.org/wiki/MS-DOS). After [Apple had
licenced the Xerox GUI and released the Apple Lisa
in 1883](https://de.wikipedia.org/wiki/Apple_Lisa), the concept
introduced with the Xerox Alto and Xerox Star became a widely accepted
standard. Microsoft also licenced the Xerox GUI and in the same year,
similar graphical user interfaces were developed for the IBM personal
computer (PC). [Microsoft introduced a computer mouse for the IBM PC
in 1983](https://tech-insider.org/personal-computers/research/1983/07.html)
and [released Microsoft Windows 1.0
in 1985](https://de.wikipedia.org/wiki/Microsoft_Windows).

After AT\&T had commercialized its UNIX System III in 1982, the source
code for the operating system UNIX could not be shared for a minimal fee
anymore. This finally led to the emergence of the open-source software
project GNU (**G**NU is **N**ot **U**NIX). Since the original UNIX-like
Kernel, GNU Hurd, is still not production-ready in 2020, almost all GNU
distributions use the Kernel "Linux" which Linus Torvalds developed in
1991. GNU/Linux distributions like Debian, Ubuntu as well as other
operating systems with Linux Kernel (Android) made open-source software
more and more popular.

## Logic gates as the building block of programmable computers

In my opinion, the evolution of switching mechanisms and miniaturization
is not the most important aspect about computers. The flexibility of
programmable, Turing-complete machines is, what makes  
them revolutionary. In all modern computers this flexibility comes from
the combination of calculations in the binary system and a vast number
of gates that allow for performing Boolean logic. It does not matter how
these logic gates are implemented in hardware (using a specific layout
of resistors, diodes and relay switches or electronic switches such as
transistors). What matters is the interplay of the logic gates. 

### Digital circuit simulation 

[The Autodesk circuits feature of the tinkercad online tool by Autodesk
(tinkercad.com)](https://www.tinkercad.com/circuits) contains a digital
circuit simulator that supports a virtual Arduino Uno Rev.3
microcontroller device as a building block that can be integrated into
virtual circuits. Programs that would run on a real Arduino Uno Rev.3
development board can be uploaded into the respective section of the
online-tool to simulate the program execution in the virtual circuit.
[The Arduino Simulator 1.5 by Xevro.be is a standalone program for
offline-usage and available for
free](https://xevro.be/products/arduino%20simulator%201.5.html).

#### An interactive virtual CPU from logic gates in Logism by R.J. Astwick 

Logism (download cs3410 Cornell version 2.7.2 from the material section
at [http://www.cs.cornell.edu/courses/cs3410/2015sp/](http://www.cs.cornell.edu/courses/cs3410/2015sp/)
) is an excellent open source, interactive digital circuit simulator. It
helps a lot in understanding electronic circuits and computer
components. You can use it to open the [virtual
central processing unit (CPU) by R.J. Astwickand Dirk
Sudholt](http://www.dcs.shef.ac.uk/intranet/archive/public/2014_2015/projects/ug/aca12rja.html)
(download-link:
[https://drive.google.com/drive/folders/0B6iPp93rMnxLfnNpTnJXWVJ5X1JfbFdCMDl5VmE3U1VWZkVoTzZSR1V6SnE2UG4tVEpGRlE](https://drive.google.com/drive/folders/0B6iPp93rMnxLfnNpTnJXWVJ5X1JfbFdCMDl5VmE3U1VWZkVoTzZSR1V6SnE2UG4tVEpGRlE)).
By clicking on the circuit components in logism, one can investigate the
underlying circuit at the level of individual gates. For your own
circuit designs, you should use [Logism
evolution cs3410 Cornell
fork](https://github.com/cs3410/logisim-evolution) version 2.12
or newer (.jar file at
[http://www.cs.cornell.edu/courses/cs3410/2017sp/resources/](http://www.cs.cornell.edu/courses/cs3410/2017sp/resources/))
which allows export as a VHDL file or Verilog file that is compatible
with the Qflow toolchain and Verilator or Aldec Active-HDL.


### How the CPU works as part of the von Neumann architecture

aaaa

<br/>

Copyright 2018 - 2020 DerAndere
