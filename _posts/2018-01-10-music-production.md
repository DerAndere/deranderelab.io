---
layout: post
title: Review of free synthesizer VST plug-ins
date:   2018-01-10 00:00:00 -0300
author: DerAndere
categories: tech
permalink: /free-synth-vst-plug-ins/
tags: [tech, open-source, software-tips, music-production]
---

**[Werbung ohne Auftrag / unpaid advertisement]**

The digital work station (DAW) I use for music recording and production is [Reaper](https://www.reaper.fm/download.php) by Cockos Inc.. It has an unrestricted evaluation period. I would like to mention my favourite synthesizer VST plug-ins here:

## 1.1  Foorius by DMI of Smart Electronix

The Demo version of [Foorius](https://www.kvraudio.com/product/foorius_by_smart_electronix) by DMI of Smart Electronix) by DMI of Smart Electronix is for free and you have to press a button every now and then to continue using it, but it is my second-to-favourite synthesizer VST plugin after Tone2 Icarus.

- 3 Oscillators. When they are set to digital oscillators, they can load custom wavetables that can be edited inside Foorius by drawing waveforms. Alternatively, oscillators can be set for resonance sweep / predefined sweeps or for pulse width modulation or they can be set as noise oscillators with 3 different types of noise.
- Oscillator mix
- flexible filters 

## 1.2 Icarus 1.5 by Tone2

My favourit synthesizer VST plugin is [Icarus 1.5 by Tone2](https://www.tone2.com/download.html). The demo can be used for free, you just have to close and reopen the user graphical user interface of the plugin (not the whole DAW) every 10 minutes to prevent the Icarus 1.5 demo from getting silenced.

- 2 wavetable Oscillators. It comes with poweful tools for spectral editing of waveforms and you can freely draw the waveforms to create custom wavetables.
- Additionaly wavetable sound can be shaped by waveshaping, harmonic content morphing, granulizing, formant shifting, phase distortion, reversing, ring modulation, unison voices
- Wavetable index sweep
- Oscillator mix
- Pulse-width modulation (PWM), frequency modulation (FM)
- Decent filters
- All envelopes, LFOs and filters can also be drawn free hand

## 1.3  XSRDO Analogy by Kirsty Roland (XSRDO)

[XSRDO Analogy](http://xsrdo.blogspot.com/) (formerly known as XSRDO_Patchwork Modular System) is a freely available modular synthesizer VST plug-in by Kirstry Roland (XSRDO).

- 25 modules can be chosen freely.
- Initialize up to 34 wavetable oscillator modules that can load custom wavetables and support wavetable index sweep
- Initialize up to 20 mixer modules for crossfading between two sources
- Decent standard filters

## 1.4  Lynx by Sami Puuronen (Xenobioz)

The [Lynx VST synthesizer plug-in by Sami Puuronen (Xenobioz)](http://www.xenobioz.com/2020/05/lynx-105-available.html) features:

- 2 oscillators that can load custom (single circle) waveforms
- Oscillator Mix for crossfading between osicllators

## 1.5   Zyn-Fusion

Zyn-Fusion is a powerful open source synth for additive and subtactive sound synthesis with a modern graphical user interface. 

- 16 Oscillators with sequential modulation (FM, phase modulation, ring modulation)
- huge unison
- formant filter flexible but dificult to set up 
- Although in principle all imaginable waveforms can be produced for each of the oscillators, achieving the desired result is difficult (waveforms cannot be drawn free hand but have to be generated from standard waveforms by spectral editing)   
- Waveforms cannot be changed during sound generation
- MIDI learn and automation is not supported for all sliders
- Open source: An unrestricted version can be built from source code on www.github.org. For building Zyn-Fusion for Microsoft Windows, use the following instruction:

First download msys2 from here: [https://www.msys2.org/](https://www.msys2.org/) and install it in the system partition (usually C:)
Then open the msys2 shell by running msys2.exe and follow the instructions at [https://www.msys2.org/](https://www.msys2.org/) to update MSYS2 and to install the [mingw-w64-x86_64-toolchain for MSYS2](https://packages.msys2.org/group/mingw-w64-x86_64-toolchain). Then install [cmake for MSYS2](https://www.msys2.org/docs/cmake/) by executing the following command in the MSYS2 shell:
```
pacman -S mingw-w64-x86_64-cmake
```
After the whole process was completed successfully, close the msys2 shell. Open the mingw64 shell (mingw64.exe) and follow the [build instructions](https://github.com/zynaddsubfx/zyn-fusion-build): Type the following and execute by pressing enter:
```
git clone --recursive https://github.com/zynaddsubfx/zyn-fusion-build zyn-fusion-build
```
Then, type the following and press enter:
```
cd zyn-fusion-build
git submodule update --init --recursive
```
Then type
```
# Install build dependencies
make MODE=release -f Makefile.mingw64.mk install_deps

# Start building
make MODE=release -f Makefile.mingw64.mk all
```
If that did not work, try the following workflow:
After the line
```
git clone --recursive https://github.com/zynaddsubfx/zyn-fusion-build zyn-fusion-build
```
Was executed in the mingw64 shell (mingw64.exe), use the Windows Explorer to navigate to the location of the bash shell script build-mingw64.sh. Open it with a text editor and change the last two lines from 
```
demo = true
release = false
```
to:
```
demo = false
release = true
```
Save the modified file build-mingw64.sh Then, in the mingw64 shell, type the following and execute by pressing enter:
```
cd zyn*
./build-mingw64.sh
```
This starts the automatic cloning of the repository [https://github.com/zynaddsubfx/zyn-fusion-build](https://github.com/zynaddsubfx/zyn-fusion-build) and builds zyn-fusion for Microsoft Windows which takes about 30 minutes. After the build process was complete and successfull, the mingw64 shell can be closed. During the build process, a directory with a portable version of Zyn-Fusion is automatically created under C:\msys64\home\<user>\zyn-fusion-build.
Open that directory and copy the folder C:\msys64\home\<user>\zyn-fusion-build\zyn-fusion-windows-64bit-release. Paste that complete folder into the directory where your DAW is looking for (VST-)plugins.

## 1.6  TX16Wx by CWITEC Music Software

[TX16Wx](https://www.tx16wx.com/) by CWITEC Music Software started as a VST software version of the Yamaha TX16W and evolved into what is probably the most powerful free software sampler: Recording samples and producing own soundfonts in .sf2 format. Great for samples of real acoustic instruments, percussion samples, vocal samples.

## 1.7  sforzando by Plogue 

[sforzando](https://www.plogue.com/products/sforzando.html) by Plogue is a free sfz player for playing soundbanks in .sfz format such as sophisticated soundbanks for piano, strings and saxophone. 

## 1.8  TAL Filter by Togu Audio Line

[TAL Filter](https://tal-software.com/products/tal-filter) (not to be confused with TAL Filter 2) by Togu Audio Line is a great active 4-pole (24db/decade) lowpass filter with drive/input control and filter resonance (Q).

## 1.9  Chameleon by Keith Bloemer (GuitarML)

[Chameleon](https://guitarml.com/#products) by GuitarML is an open source amplifier simulator (amp sim) that allows to simulate clean sounds from tube amplifiers as well as distorted (overdrive) sounds. Machine learning is used to provide additional tones: The community can train the neural network ([a real-time implementation of a stateful LSTM neural network](https://github.com/Alec-Wright/Automated-GuitarAmpModelling)). [Technical documentation was published in a series of articles with the title "Neural Networks for Real-Time Audio"](https://towardsdatascience.com/neural-networks-for-real-time-audio-stateful-lstm-b534babeae5d). The training data yields new tone models that can be downloaded from the [NeuralPi tone library](https://guitarml.com/tonelibrary/tonelib-npi.html). To replace the original tones, simply overwrite the "red.json", "gold.json", or "green.json" file with the user created model of the same name before running Chameleon. To reset to the originals, delete the custom model. The next time you run Chameleon, the original tones will be written to the tone directory.

The locations of the json tone files are:

Windows 10: C:/Users/\<username\>/Documents/GuitarML/Chameleon/tones
Mac:  /Users/\<username\>/Documents/GuitarML/Chameleon/tones

# 2      Using Icarus by Tone2 

TODO

<br/>

Copyright 2018 - 2020 DerAndere
