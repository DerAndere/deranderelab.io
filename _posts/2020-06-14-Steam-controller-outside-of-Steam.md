---
layout: post
title: Using the programmable Steam Controller outside of Steam
date:   2020-06-14 00:00:00 -0300
author: DerAndere
categories: tech
permalink: /glosc/
tags: [tech, open-source, software-tips]
---

**[Werbung ohne Auftrag / unpaid advertisement]**

To use the programmable [Steam Controller](https://store.steampowered.com/app/353370/Steam_Controller/) by Valve with non-Steam games and other programs outside of Steam, some steps have to be followed. 
This was tested for the freely available rallye racing simulation game "RBRPro" (A freely available version of the game Richard Burns Rally with several mods, including the RBRPro manager, Next Generation Physics 6 mod (NGP 6), [czech tournament plug-in and track pack](http://rbr.onlineracing.cz/index.php?act=al&p=howtostart&lng=eng), virtual reality (VR) mod, RBR FMOD).
For programs that support Xinput controllers (Xbox controllers), it might be sufficient to follow steps 8-18:

1. Download RBRPro from the Dropbox-link posted in "files" section of the [Facebook group "RBRPro LiveStream"](https://www.facebook.com/groups/363508084337823)
1. Install the game (RBRPro) (extract the zip archive using e.g. 7zip)
1. Run the executable RBRProManager.exe and fully update the RBRPro manager and contents (cars, stages, co-drivers). Then, close RBRPro.
1. Install [vJoy](http://vjoystick.sourceforge.net/site/)
1. Run vJoyConfig.exe and enable vJoy Stick 1 with 18 buttons and axes X,Y,Z,Rx,Ry,Rz. 
1. Reboot the computer
1. Install Universal Control Remapper (UCR) which is used to feed vJoy with user input. The [original version of UCR](https://github.com/evilC/UCR) is using AutoHotKey and supports remapping of the mouse delta axes to controller axes out of the box, while the C\# rewrite [UCR 0.9.0](https://github.com/Snoothy/UCR/releases) uses the .NET framework and has better XInput (Xbox-controller) support via the provider "ViGEm".
1. Install the Steam client software
1. Right-click on the executable Steam.exe and select "Run as Administrator" to run the Steam client bootstrapper with administrator rights. Log in to your Steam account. 
1. Open Steam in Big Picture Mode
1. Connect the Steam controller and the computer
1. In Steam Big Picture Mode, go to Settings -\> Basic Controller Configuration. Enable "Generic Controller support" and "Xbox Controller support". Go through all configurations, go to "Explore configs" -\> Templates -\> Gamepad -\> "Save"
1. Go to Settings -\> "Configure Steam controller" -\> Explore configs -\> Templates -\> Gamepad -\> "Save")
1. Add any program (executable .exe) as a non-Steam game to your Steam library
1. Install [GloSC](https://github.com/Alia5/GloSC/releases)
1. Right-click on the executable GloSC.exe and select "Run as Administrator"
1. Follow the instructions to add the non-Steam program UCR.exe as a so-called "GloSC-shortcut" with default settings to your Steam library using GloSC. You may have to do this while you are logged in to Microsoft Windows with an Administrator account. The Steam client bootstrapper has to be running in the background with Administrator rights (see above). Note: For games that support Xinput controllers, UCR and vJoy (steps 1-7) are not strictly required: You can add GloSC-shortcuts to such games to the Steam library and in the Steam client, select "play" on the GloSC-shortcut to those games. You may have to minimize the Steam client bootstrapper and the Steam overlay and trun the corresponding executable (.exe) of the game from the Windows Explorer.
1. Reboot the computer. For the remaining steps, no administrator rights are required.
1. Run the executable Steam.exe to start the Steam client bootstrapper. Log in to your Steam account.
1. In the Steam client, verify that your Steam Controller is detected as a Steam Controller. If not, reconnect the Steam Controller.
1. In the Steam client, go to library, select the GloSC-shortcut to UCR and select "Play"
1. If UCR does not start, minimize the Steam client and use the Windows Explorer or PowerShell.exe to run the executable UCR.exe
1. In UCR, create a new profile
1. In UCR, add Remaps for all desired Steam controller buttons (select "Bind" or "Xbox Controller 1" controls) to vJoy Stick 1 controls.
1. Optionally, you can verify that the setup works by running the executable vJoyMonitor.exe and pressing buttons on the Steam Controller.
1. Run RBRProManager.exe.
1. In the RBRPro Manager, go to Options -\> Controls -\> Monitor Peripherals. Select "vJoy" from the dropdown list.
1. In the RBRPro Manager, go to Stages and add the desired tracks to a free list (on the right) 
1. In the RBRPro Manager, go to Cars, select NGP 6 and add the desired cars that support NGP6 to a free list (on the right).
1. In the RBRPro Manager, go to "OFFLINE MODE" (bottom right) to launch RBR_SSE.exe. 
1. In RBR_SSE, use arrow keys, Enter and Esc (or use buttons that are mapped to those keys) to navigate to Options -\> "Control" -\> "Control Settings. Adjust settings.
1. Go to Options -\> Plugins -\> Tournament -\> Shakedown to start a race on a single track (stage).

<br/>

Copyright 2020 DerAndere
