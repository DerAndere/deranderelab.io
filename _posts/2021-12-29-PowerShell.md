---
layout: post
title:  "Using the PowerShell"
date:   2021-12-29 00:00:00 -0300
author: DerAndere
categories: programming
permalink: /powershell/
tags: [programming, tech] 
---

## How to use the PowerShell

There are manny options to run an executable (EXE, file extension: *.exe) in Microsoft Windows. You can use the explorer to navigate to the executable and double-click on it. Or you can use the legacy CMD.exe. An alternative is to use the PowerShell. There are manny options how to run an executable from the PowerShell but the best solution in most situations is to change directory and use the Start-Process cmdlet. To change directory:
```
Set-Location -FilePath "C:\path\to\executables parentdirectory"
```
or shorter:
```
Set-Location "C:\path\to\executables parentdirectory"
```
or even shorter using the alias cd:
```
cd "C:\path\to\executables parentdirectory"
```
To move one directory up, use
```
Set-Location ..
```
or 
```
cd ..
```
To list directory contents:
```
Get-ChildItem "C:\path\to\executables parentdirectory"
```
or
```
Set-Location "C:\path\to\executables parentdirectory"
Get-ChildItem
```
or use the alias:
```
cd "C:\path\to\executables parentdirectory"
ls
```
To execute an executable, use the [Start-Process cmdlet](https://docs.microsoft.com/de-de/powershell/module/Microsoft.PowerShell.Management/Start-Process?view=powershell-7.2):
```
Start-Process -FilePath "C:\path\to\executables parentdirectory\executable name.exe"
```
or shorter
```
Start-Process "C:\path\to\executables parentdirectory\executable name.exe"
```
or
```
Set-Location "C:\path\to\executables parentdirectory"
Start-Process "executable name.exe"
```
If the environment variables were edited so that the executable was added to the System Path environment variable, that executable can be run directly from any directory with the Start-Process command:
```
Start-Process "executable name.exe"
```

<br/>

Copyright 2021 - 2022 DerAndere
