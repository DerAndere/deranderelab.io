---
layout: post
title: Canon printer error B200
date:   2025-02-15 00:00:00 -0300
author: DerAndere
categories: tech
permalink: /Canon-printer-error-B200
tags: [tech]
---

Canon printers like the Canon Pixma MG5150 might show error message B200.
The error B200 is meant to indicate a problem with the print head. However, different causes are possible.
In my case, the following steps helped to resolve the error:

1. Switch off the printer by pressing the power button.
1. Open the cover of the printer to inspect the ink cartridges and print head by lifting the glas bed of the scanner.
1. Switch on the printer by pressing the power button.
1. After the print head stays parked in the maintenance position for 30 seconds, switch off the printer by pressing the power button.
1. Unplug the printer from the mains power. Press the power button and hold for 30 seconds, then release the power button.
1. Connect the printer with the mains power (plug it in).
1. Switch on the printer by pressing the power button. While the print head moves, close the cover of the printer by lowering the glas bed of the scanner.

If the error message B200 reapears, repeat the steps above. In addition, after step 5: Replace nearly empty ink cartridges. If required, perform the following additional steps:

a. remove all ink cartridges.

b. lift the lever to release the print head.

c. Remove the print head and carefully clean it with warm water.

d. Optionally, carefully clean the electrical contact pads with isopropanol.

e. Make sure the print head is dry.

f. Re-insert the print head and close the lever to lock the print head in place.

g. Insert all ink cartridges.

h. Continue with step 6. 

If the error message B200 still apears, repeat the steps above and cover the following electrical contact pads with electrical insulating tape: top-left, top-right, (optionally: one or both of the bottom-left pads). Then, continue with step 6. 


