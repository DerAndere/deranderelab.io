---
layout: post
title: Thermodynamics
date:   2020-08-29 00:00:00 -0300
author: DerAndere
categories: data-analysis
permalink: /thermodynamics/
tags: [data-analysis, science, chemistry, physics]
---

Understanding the principles of thermodynamics is of interest for multiple scientific disciplines. Thermodynamic analysis is the bread and butter of many physicists and engineers. For them, it is usually unproblematic to understand the basics of thermodynamics because of their strong background in mathematics. On the other end of the spectrum, chemists also frequently have to analyze thermodynamics in order to predict and optimize chemical reactions, and biologists also come into contact with the topic in the context of toxycology, enzymology or metabolic flux analysis. For this audience it is often not easy to thoroughly understand the concepts of thermodynamics. To make things worse, in chemistry text books like "Physical Chemistry" (Atkins 2006), the considered thermodynamic systems are not always defined rigorously enough. The following unreviewed article is a summary of the authors understanding of the topic as a graduate student in biochemistry. This text is provided without guarantees of completeness or accuracy. 


## Thermodynamics in reactive open systems

A scientific law is a statement that describes what phenomena happen (observed or predicted). Overlap between scientific theory and scientific laws allows for predictions. By repeated successful predictions, the appropriate field of application for the law can be extended. If we consider a system which together with its surroundings constitutes a larger, isolated system (the universe), one important law is the [law of conversion of energy](https://en.wikipedia.org/wiki/Conservation_of_energy) which states that the energy of the universe is constant. Thus

\\[
\dd{E\_{universe}} = \dd{E\_{system}} + \dd{E\_{surroundings}} = 0
\label{eq:ConsE}
\\]

where $E\_{system}$ the energy of the system, which is the sum of the system's kinetic energy $E\_{kin}$, the system's potential Energy $E\_{pot}$ and the system's internal energy $U$. Thus

\\[
\dd{E\_{system}} = \dd{E\_{kin}} + \dd{E\_{pot}} + \dd{U}.
\label{eq:2}
\\]

If the kinetic energy of the system as a whole and potential Energy of the system as a whole (e.g. due to interaction with an external gravitational field) is not considered, i.e. for $\dd{E\_{kin}} = 0$, $\dd{E\_{pot}} = 0$, it follows that $\dd{E\_{system}} = \dd{U}$.  

The [zeroth law of thermodynamics](https://en.wikipedia.org/wiki/Zeroth_law_of_thermodynamics) states that if two thermodynamic systems are each in thermal equilibrium with a third one, then they are in thermal equilibrium with each other. Maxwell expressed it in the words "All heat is of the same kind" [(Maxwell, Clerk 1871)](https://archive.org/details/bub_gb_Z23vxaOhW0sC). It can also [expressed by saying that there is precisely one kind of non-mechanical, non-matter-transferring contact equilibrium between thermodynamic systems](https://en.wikipedia.org/wiki/Zeroth_law_of_thermodynamics).

<br>

The most general case that can be considered is a reactive, open system that can exchange energy and matter with its surroundings. For an open, reaktive system where elementary chemical reactions k take place the first principle of thermodynamics can be expressed as

\\[
\dd{U(S,V,N)} = \dd{\Phi} + \variation{w}
\label{eq:2b}
\\]

with $\dd{\Phi}$: General heat, and

$\variation{w}$: infinitesimally small change in work done by the system (inexact derivative)
<br><br>

The second principle of thermodynamics can then be expressed  as ([Petrucci et al. 2016, p.600](https://www.pearsoncanada.ca/media/highered-showcase/multi-product-showcase/petrucci_ch13.pdf)):

\\[
\dd{S\_{universe}} = \dd{S\_{system}} + \dd{S\_{surroundings}} \ge 0 .
\label{eq:SecondLTDj}
\\]                                       

For reversible processes, 

\\[
\dd{S\_{universe}} = \dd{S\_{system}} + \dd{S\_{surroundings}} = 0
\label{eq:SecondLTDk}
\\]                                       

For irreversible processes, 

\\[
\dd{S\_{universe}} = \dd{S\_{system}} + \dd{S\_{surroundings}} > 0
\label{eq:SecondLTDl}
\\]                                       

According to the second principle of thermodynamics ([De Decker 2015](https://www2.ulb.ac.be/sciences/nlpc/pdf/15.De_Decker_PA.pdf)),

\\[
\dd{S} = \dd{S\_{int}} + \sum\_i{(\dd{S\_{e,i}})}; \dd{S\_{int}} \ge 0 ,
\label{eq:SecondLTD}
\\]

with $\dd{S\_{int}}$: infinitesimally small change in entropy due to internal processes (caused by chemical reactions), and

$\dd{S\_{e,i}}$: infinitesimally small entropy, tranfered from the connected system i through mass exchange of compound i.

\\[
\sum\_i{\dd{S\_{e,i}}} = \frac{\dd{\Phi}}{T} - \frac{1}{T} \sum\_k{(\mu\_k \dd{n\_{e,k}})}
\label{eq:SecondLTDg}
\\]

\\[
\dd{S\_{int}} = - \frac{1}{T} \sum\_k{\mu\_k \dd{n\_{int,k}}}
\label{eq:SecondLTDh}
\\]

Thus

\\[
\dd{S} = \frac{\variation{\Phi}}{T} - \frac{1}{T} \sum\_k{(\mu\_k \dd{n\_{e,k}})} - \frac{1}{T} \sum\_k{(\mu\_k \dd{n\_{int,k}})}
\label{eq:SecondLTDi}
\\]

If the heat $\variation{q} = T \dd{S} + \sum\_k{(\mu\_k \dd{n\_{int,k}})}$ is introduced, it follows ([De Decker 2015](https://www2.ulb.ac.be/sciences/nlpc/pdf/15.De_Decker_PA.pdf)) that

\\[
\dd{\Phi} = \variation{q} + \sum\_k{(\mu\_k \dd{n\_{e,k}})}
\label{eq:2c}
\\]

Here, $\variation{q}$: infinitesimally small change in heat (inexact derivative).

<br>

Inserting \eqref{eq:2c} into \eqref{eq:2b} yields

\\[
\dd{U(S,V,N)} = \variation{q} + \variation{w} + \sum\_k{\mu\_k \dd{n\_{e,k}}} 
\label{eq:2d}
\\]

Different approaches towards a thermodynamic description of systems far from equilibrium ([Jazinsky 2011](https://www.annualreviews.org/doi/10.1146/annurev-conmatphys-062910-140506); [Esposito and Van den Broeck 2011](https://iopscience.iop.org/article/10.1209/0295-5075/95/40004#); [Prokovskii 2013](http://dx.doi.org/10.1155/2013/906136)) and description of chemically reactive systems were presented in recent literature ([De Decker 2015](https://www2.ulb.ac.be/sciences/nlpc/pdf/15.De_Decker_PA.pdf); [Rao and Esposito 2016](https://journals.aps.org/prx/abstract/10.1103/PhysRevX.6.041064)). 

[Prigogine introduced the concept of internal variables $\xi_i$ for discussion of non-equilibrium thermodynamics (Prigogine 1955; Prigogine 1967). The energy dissipation is caused by entropy production due to relaxation of internal variables which upon relaxation to discuss thermodynamics of chemical reactions. Prigogine showed that in the case of chemical reactions, the internal parameter $\xi$ is a measure of the extend of reaction](https://en.wikipedia.org/wiki/Non-equilibrium_thermodynamics) (Prigogine 1955; Prigogine 1967). The infinitesimally small change in the extend of reaction is defined as 

\\[
\dd{\xi} = \frac{\dd{n\_i}}{\nu\_i}
\label{eq:2e}
\\]

with $\nu\_i$: stochiometric number (stochiometric coefficient) of reactand i.

<br>

Prigogine showed that for chemically reactive systems, the extend of reaction $\xi$ From  $\Delta\\!\xi = \frac{\Delta\\!n\_i}{\nu\_i}$ and [from the fact that at the beginning of the reaction by definition $\nu\_i = 0$ follows that $\Delta\\!\xi = \xi$. Therefore](https://en.wikipedia.org/wiki/Chemical_thermodynamics):

\\[
\\xi = \frac{\mathop{\kern0pt \mathrm{\Delta}}\\!n\_i}{\nu\_i}
\label{eq:2f}
\\]

The rate of reaction is ([Davis and Davis 2003](https://resolver.caltech.edu/CaltechBOOK:2003.001))

\\[
r = \dv{\xi}{t}
\label{eq:2g}
\\]

The vector $J$ whose components are all reaction rates $r\_i$ relevant for the system under consideration ([De Decker 2015](https://www2.ulb.ac.be/sciences/nlpc/pdf/15.De_Decker_PA.pdf); [Pekar 2018](https://doi.org/10.3389/fchem.2018.00035)) looks as if it could be the reaction flux.

## Unreactive open systems

For an unreactive open system that is connected to other systems via different walls that are only permeable to either heat (diathermic wall), or work (piston for adiabatic work) or matter of a specific compound i (semipermeable membrane that is permeable only by compound i) the internal energy of the open system is [(Knuiman et al. 2012)](https://pubs.acs.org/doi/full/10.1021/ed200405k):

\\[
\dd{U(S,V,N)\_o} = \dd{U\_c} + \sum\_i{\dd{U\_{em,i}}}
\label{eq:3}
\\]

with $\dd{U(S,V,N)\_{em,i}}$: infinitesimally small internal energy, transfered from the connected system i through mass exchange of compound i and

$\dd{U(S,V,N)\_c}$: inner engery of the whole closed system.

<br>

According to the [first law of thermodynamics](https://en.wikipedia.org/wiki/First_law_of_thermodynamics),

\\[
\dd{U(S,V,N)\_o} = \variation{q} + \variation{w} + \sum\_i{\dd{U\_{em,i}}}
\label{eq:4}
\\]

The mass exchange is quantized, therefore the last equation becomes

\\[
\dd{U(S,V,N)\_o} = \variation{q} + \variation{w} + \sum\_i{u\_i\dd{n\_i}},
\label{eq:5}
\\]

with $n\_i$: amount of substance of type i

$u\_i$: internal energy per unity of amount of substance i.

Equation \eqref{eq:5} makes clear that change in internal energy cannot always be uniquely resolved into heat and work components [Knuiman et al. 2012](https://pubs.acs.org/doi/full/10.1021/ed200405k).

### Quasistatic processes and reversible processes

If the process of mass exchange is quasistatic or even reversible (quasistatic, no dissipation), the expression for the work is

\\[
\variation{w} = \variation{w\_o} + \variation{w\_{flow}}.  
\label{eq:5b}
\\]

Since for the given process $\variation{w\_{flow}} = \sum\_i{p\_{I,i} v\_i \dd{n\_i}}$ ([Knuiman et al. 2012](https://pubs.acs.org/doi/full/10.1021/ed200405k)) the expression for the work becomes 

\\[
\variation{w} = \variation{w\_o} + \sum\_i{p\_{I,i} v\_i \dd{n\_i}}.  
\label{eq:5c}
\\]

When the mass exchange is a quasistatic (or even reversible) transfer, the second law of thermodynamics holds:

\\[
\dd{S} = \frac{\variation{q}}{T} + \sum\_i{\dd{S\_{em,i}}}
\label{eq:SecondLTDqs} 
\\]

The mass exchange is quantized, hence

\\[
\dd{S} = \frac{\variation{q}}{T} + \sum\_i{s\_i \dd{n\_i}}
\label{eq:SecondLTDb} 
\\]

with $s\_i$: Entropy per unity of amount of substance i

<br>

Inserting \eqref{eq:5c} and \eqref{eq:SecondLTDb} into \eqref{eq:5} yields 

\\[
\dd{U(S,V,N)\_o} = T \dd{S\_o} + \sum\_i{s\_i \dd{n\_i}} + \variation{w\_o} + \sum\_i{p\_{I,i} v\_i \dd{n\_i}} + \sum\_i{u\_i \dd{n\_i}} 
\label{eq:6}
\\]

In equation \eqref{eq:6} the contributions of exchanged heat and work are still seperate. 

That seperation vanishes when the equation is simplified to 

\\[
\dd{U(S,V,N)\_o} = T \dd{S\_o} + \variation{w\_o} + \sum\_i{\mu\_i \dd{n\_i}}
\label{eq:SecondLTDo}
\\]

with $\mu\_i$: chemical potential of component i, $\mu\_i = (\dv{G}{n\_i})\_{p,T,n'}$ 

and $\variation{w\_o} = \sum\_j{X\_j \dd{a\_{o,j}}}$.

<br>

Notably, in equation \eqref{eq:SecondLTDo}, the first term does not exactly (uniquely) correspond to the exchanged heat (see \eqref{eq:SecondLTDqs}). Similarly, the term involving the chemical potential is neither a pure work term nor $\sum\_i{\dd{U\_{em,i}}}$ (see \eqref{eq:6}). Rather, there are mixed contributions of heat and work in all terms of equation \eqref{eq:SecondLTDo}.

<br>

For any system and any general process the Gibbs equation is: 

\\[
\dd{U(S,V,N)} = T \dd{S} + \sum\_j{X\_j \dd{a\_j}} + \sum\_i{\mu\_i \dd{n\_i}},
\label{eq:7}
\\]

Equation \eqref{eq:7} still [holds, if the transfer of energy is not quasistatic, but in that case the equation cannot easily be derived from the first and second law of thermodynamics, because those laws are only defined for quasistatic processes in certain systems or for any process in a closed system](https://en.wikipedia.org/wiki/First_law_of_thermodynamics). Thus the [first term does not necessarily correspond to pure heat ($\variation{q} \neq T \dd{S}$) and the later terms do not necessarily correspond to pure work ($\variation{w} \neq \sum\_j{X\_j \dd{a\_j}}$)](http://cvika.grimoar.cz/callen/) ([Callen 1985, p97](https://cvika.grimoar.cz/callen/callen_04.pdf)).

## Fundamental equations of thermodynamics

\\[
H(S,p,N) = U(S,V,N) + p V
\label{eq:Enthalpy}
\\]

\\[
F(T,V,N) = U(S,V,N) – T S 
\label{eq:HelmholzFE}
\\]

\\[
G(p,T,N) = H – T S = U + p V – T S
\label{eq:GibbsFEnth}
\\]

For any system and any general process the Gibbs equation is given by \eqref{eq:7}. With $X\_{Vol} = -p$ and $a\_{Vol} = V$, the work can be seperated into volume-work and non-volume-work: $\variation{w} = \variation{w\_{Vol}} + \variation{w'} = -p \dd{V} + \sum\_l{Y\_l \dd{y\_l}}$. Thus

\\[
\dd{U(S,V,N)} = T \dd{S} - p \dd{V} + \sum\_l{Y\_l \dd{y\_l}} + \sum\_i{(\mu\_i \dd{n\_i})},
\label{eq:7b}
\\]

Insertion of  \eqref{eq:Enthalpy} into \eqref{eq:7b} yields

\\[
\dd{H(S,p,N)} = T \dd{S} + V \dd{p} + \sum\_l{Y\_l \dd{y\_l}}  + \sum\_i{(\mu\_i \dd{n\_i})}
\label{eq:dH}
\\]

Insertion of  \eqref{eq:HelmholzFE} into \eqref{eq:7b} yields

\\[
\dd{F(T,V,N)} = -S \dd{T} - p \dd{V} + \sum\_l{Y\_l \dd{y\_l}} + \sum\_i{(\mu\_i \dd{n\_i})}
\label{eq:dF}
\\]

Insertion of \eqref{eq:GibbsFEnth} into \eqref{eq:7b} yields

\\[
\dd{G(p,T,N)} = -S \dd{T} + V \dd{p} + \sum\_l{Y\_l \dd{y\_l}} + \sum\_i{(\mu\_i \dd{n\_i})}
\label{eq:dG}
\\]                                       

From \eqref{eq:dG} follows with the second  principle of thermodynamics, \eqref{eq:SecondLTD}, that $\dd{G} \le \variation{w'}$. If the system is at equilibrium (which requires that all processes under consideration are reversible), $dG = \variation{w'}$, else $dG < \variation{w'}$.

<br>

For homogenous macroscopic systems, it follows from equation \eqref{eq:7} according to Eulers homogeneous function theorem that 

\\[
U(S,V,N) = T S – p V + \sum\_i{(\mu\_i \dd{n\_i})}
\label{eq:U}   
\\]

Insertion of \eqref{eq:U} into \eqref{eq:Enthalpy} yields 

\\[
H(S,p,N) = T S + \sum\_i{(\mu\_i \dd{n\_i})}
\label{eq:HTS}   
\\]

Insertion of \eqref{eq:U} into \eqref{eq:GibbsFEnth} yields 

\\[
G(p,T,N) = \sum\_i{(\mu\_i \dd{n\_i})}
\label{eq:Gmudni}   
\\]

## Spontaneous irreversible processes and reversible processes

From the second law of thermodynamics \eqref{eq:SecondLTDj} follows the inequality for spontaneous change ([Petrucci et al. 2016](https://www.pearsoncanada.ca/media/highered-showcase/multi-product-showcase/petrucci_ch13.pdf); [Nelson et al. 2008](https://ocw.mit.edu/courses/chemistry/5-60-thermodynamics-kinetics-spring-2008/)):

\\[
\dd{S} \ge \frac{\variation{q}}{T}
\label{eq:Clausius}
\\]                                       

For reversible processes,

\\[
\dd{S} = \frac{\variation{q}}{T} .
\label{eq:ClausiusRev}
\\]                                       

Irreversible processes are spontaneous if

\\[
\dd{S} > \frac{\variation{q}}{T}
\label{eq:ClausiusIrr}
\\]                                       

Reorganizing \eqref{eq:ClausiusIrr} and inserting it into \eqref{eq:2d} yields the following inequality that holds for spontaneous changes ([Nelson et al. 2008](https://ocw.mit.edu/courses/chemistry/5-60-thermodynamics-kinetics-spring-2008/)): 

\\[
\dd{U(S,V,N)} - T \dd{S} - \variation{w} - \sum\_k{(\mu\_k \dd{n\_{e,k}})} < 0 
\label{eq:ClausiusIrrb}
\\]

with $\variation{w} = \variation{w\_{Vol}} \variation{w'} = -p \dd{V} + \sum\_l{(Y\_l \dd{y\_l})}$. Seperation of volume work $\variation{w\_{Vol}} and non-volume-work \variation{w'} yields 

\\[
\dd{U(S,V,N)} - T \dd{S} + p \dd{V} - \variation{w'} - \sum\_k{(\mu\_k \dd{n\_{e,k}})} < 0 .
\label{eq:ClausiusIrrc}
\\]

For an isolated system ($\variation{q} = 0$, $\variation{w} = 0$, $\dd{V} = 0$, $\dd{n\_{e,k}} = 0$, $\dd{U} = 0$):

\\[
(\dd{S})\_{U,V} \ge 0 .
\label{eq:Sisolated}
\\]

For closed systems with varying constant parameters, the inequality for spontaneous change is given below. Indices indicate constant parameters.

\\[
(\dd{U(S,V,N)})\_{S,V} \le 0 .
\label{eq:UScVc}
\\]

\\[
(\dd{H(S,p,N)})\_{S,p} \le \variation{w'}
\label{eq:HScpc}
\\]

\\[
\mathop{\kern0pt \mathrm{\Delta\_r}}\\!{H} = \left( \pdv{H(S,p,\xi)}{\xi} \right)\_{S,p} \le \variation{w'}
\label{eq:DrH}
\\]

\\[
(\dd{S})\_{H,p} \ge \variation{w'}
\label{eq:SHcpc}
\\]

\\[
\mathop{\kern0pt \mathrm{\Delta\_r}}\\!{S} = \left( \pdv{S}{\xi} \right)\_{H,p} \ge \variation{w'}
\label{eq:DrS}
\\]

\\[
(\dd{F(T,V,N)})\_{V,T} \le \sum\_l{(Y\_l \dd{y\_l})}
\label{eq:FVcTc}
\\]

\\[
\mathop{\kern0pt \mathrm{\Delta\_r}}\\!{F} = \left( \pdv{F(T,V,\xi)}{\xi} \right)\_{T,V} \le \variation{w'}
\label{eq:DrF}
\\]

\\[
(\dd{G(p,T,N)})\_{p,T} \le \variation{w'}
\label{eq:GpcTc}
\\]

\\[
\mathop{\kern0pt \mathrm{\Delta\_r}}\\!{G} = \left( \pdv{G(p,T,\xi)}{\xi} \right)\_{p,T} \le \variation{w'}
\label{eq:DrG}
\\]

The changes in the potentials of thermodynamics above are equal to $\variation{w'}$ if, and only if the system is at thermodynamic equilibrium, which requires that all processes under consideration are reversible.

Confusingly, in the equations above, the symbol $\mathop{\kern0pt \mathrm{\Delta\_r}}$ stands for the partial derivative against the extend of reaction, not for a difference. 

## Generalized work

In any case, the relevant generalized work terms relevant for the system and process under consideration are required:

\\[
\dd{w} = \sum\_j{X\_j \dd{a\_j}},
\label{eq:8}
\\]

where $X\_j$: Generalized force (intrinsic variable) conjugated with the extrinsic parameter $a\_j$ .

<br>

The most relevant intrinsic and extrinsic variables are given in the table below. For details, see ([Balmer 2011](https://doi.org/10.1016/B978-0-12-374996-3.00004-X)).

<br>

| Type of work  | Symbol | Generalized force | extrinsic parameter |
|---------------|--------|-------------------|---------------------|
|volume-pressure|$\variation{w\_{vol}}$|pressure ($p$)|volume ($V$)|
|electrochemical|$\variation{w\_{e}}$|electromotive force ($\varepsilon$)|charge ($Q$)|

<br>

In the context of electrochemical cells, the electromotive force is called source voltage. It is equal to the electric potential
difference for zero current through the cell [IUPAC 2019](https://doi.org/10.1351/goldbook).

<br>

## Closed systems and unreactive systems

For closed systems $\dd{n\_{e,i}} = 0$ and therefore also $\sum\_i{(\mu\_i \dd{n\_i})} = 0$.

Thus, from \eqref{eq:7} follows for closed systems:

\\[
\dd{U} = T \dd{S} – \sum\_j{(X\_j \dd{a\_j})}
\label{eq:dUc}
\\]

For an isochoric system ($\dd{V} = 0$) where no no work other than volume work is performed ($\variation{w} = -p \dd{V}$):

\\[
\dd{H} = \variation{q}
\label{eq:dHqc}
\\]

For an isobaric system ($\dd{p} = 0$), where no no work other than volume work is performed ($\variation{w} = -p \dd{V}$):

\\[
\dd{H} = dU + p \dd{V}
\label{eq:dHdUpdVc}
\\]

For an isobaric system, if temperature is held constant ($\dd{p} = 0$, $\dd{T} = 0$), the free reaction enthalpy is defined as

\\[
\mathop{\kern0pt \mathrm{\Delta\_r}}\\!G = \left( \pdv{G(p,T,\xi)}{\xi} \right) \_{p,T} 
\label{eq:FreeReactionEnth}
\\]

Confusingly, the symbol $\mathop{\kern0pt \mathrm{\Delta\_r}}$ stands for the partial derivative against the extend of reaction, not for a difference. 

Inserting \eqref{eq:Gmudni} into \eqref{eq:FreeReactionEnth} yields

\\[
\Delta\_r\\!G = \left( \pdv{G(p,T,\xi)}{\xi} \right)\_{p,T} = \left( \pdv{\sum\_i{\mu\_i \dd{n\_i}}}{\xi} \right)\_{p,T} .
\label{eq:10}
\\]

With \eqref{eq:2e}, which after reorganizing reads $\dd{n\_i} = \nu\_i \dd{\xi}$, \eqref{eq:10} becomes  

\\[
\mathop{\kern0pt \mathrm{\Delta\_r}}\\!{G} = \left( \pdv{G(p,T,\xi)}{\xi} \right)\_{p,T} = \sum\_i{(\nu\_i \mu\_i)}
\label{eq:11}
\\]

The chemical potential of substance i can be calculated from two seperate terms:

\\[
\mu\_i = \mu\_i^{o} + R T \ln{A}
\label{eq:12}
\\]

with $\mu\_i^{o}$: standard chemical potential, the chemical potential of substance i at standard conditions (IUPAC Standard Atmospheric Temperature and Pressure, SATP: pressure $p = 100 \ \mathrm{kPa}$, temperature $T = 298.15 \ \mathrm{K}$, concentration $c = 1.0 \cdot 10^{-3} \ \mathrm{mol}\,\mathrm{m}^{-3}$. NIST uses Normal Temperature and Pressure, NTP: pressure $p = 101.325 \ \mathrm{kPa}$, temperature $T = 293.15 \ \mathrm{K}$, concentration $c = 1.0 \cdot 10^{-3} \ \mathrm{mol}\,\mathrm{m}^{-3}$).

Inserting \eqref{eq:12} into \eqref{eq:11} yields

\\[
\mathop{\kern0pt \mathrm{\Delta\_r}}\\!{G} = \dd{G^{o}} + R T \ln{ \left(\prod\_i{A\_i^{\nu\_i}} \right) }
\label{eq:13}
\\]

with $\dd{G^{o}}$: Standard free reaction enthalpy, $\dd{G^{o}} = \sum\_i{(\nu\_i \mu\_i^{o})}$

For a system at thermodynamic equilibrium, $\dd{G} = 0 $. Hence, in equilibrium 

\\[
\dd{G^{o}} = -R T \ln{K}
\label{eq:K}
\\]

with $K$: Equilibrium constant, $K = \prod\_i{(A\_{eq,i}^{\nu\_i})}$ , and

$A\_{eq,i}$: Equilibrium activity of component i. 

### Electrochemical thermodynamics - Nernst equation

Equation \eqref{eq:dG} reads

\\[
\dd{G(p,T,N)} = -S \dd{T} + V \dd{p} + \sum\_l{Y\_l \dd{y\_l}} + \sum\_i{(\mu\_i \dd{n\_i})}
\label{eq:dGb}
\\]                                       

For an isochoric system ($\dd{V} = 0$) at constant temperature ($\dd{T} = 0$), if $\variation{w'} = \variation{w\_e}$ and no other non-volume-pressure work is performed, \eqref{eq:FVcTc} yields  

\\[
\dd{F(T,V,N)} \le \variation{w\_e}
\label{eq:dFe}
\\]

For an isobaric system ($\dd{p} = 0$) at constant temperature ($\dd{T} = 0$), if $\variation{w'} = \variation{w\_e}$ and no other non-volume-pressure work, \eqref{eq:GpcTc} yields  

\\[
\dd{G(p,T,N)} \le \variation{w\_e}
\label{eq:dGe}
\\]

When the reaction proceeds for $\dd{\xi}$, 

\\[
\dd{Q} = - e \dd{N \mathrm{(electrons)}} = - N\_A e \dd{n \mathrm{(electrons)}} 
\label{eq:dQe}
\\]

Inserting \eqref{eq:2e} yields 

\\[
\dd{Q} = - N\_A e \nu \mathrm{(electrons)} \dd{\xi} = -F \nu \mathrm{(electrons)} \dd{\xi}
\label{eq:Faraday}
\\]

with $F$: Faraday constant, $F = N\_A e$.

Inserting \eqref{eq:2e} into \eqref{eq:dGe}

\\[
\dd{G(p,T,\xi)} \le - \nu \mathrm{(electrons)} F \varepsilon \dd{\xi}
\label{eq:dGemf}
\\]

and

\\[
\mathop{\kern0pt \mathrm{\Delta\_r}}\\!{G} = \left( \pdv{G(p,T,\xi)}{\xi} \right)\_{p,T} \le \left( \pdv{(- \nu \mathrm{(electrons)} F \varepsilon \dd{\xi})}{\xi} \right)\_{p,T} = - \nu \mathrm{(electrons)} F \varepsilon
\label{eq:dGemfb}
\\]

For reversible processes, 

\\[
\mathop{\kern0pt \mathrm{\Delta\_r}}\\!{G} = \left( \pdv{G(p,T,\xi)}{\xi} \right)\_{p,T} = - \nu \mathrm{(electrons)} F \varepsilon
\label{eq:dGemfc}
\\]

Reorganizing yields:

\\[
\varepsilon = - \frac{\mathop{\kern0pt \mathrm{\Delta\_r}}\\!{G}}{\nu \mathrm{(electrons)} F}
\label{eq:emfDrG}
\\]

From \eqref{eq:13} follows 

\\[
\varepsilon = - \frac{\mathop{\kern0pt \mathrm{\Delta\_r}}\\!{G^{o}}}{\nu \mathrm{(electrons)} F} - \frac{R T}{\nu \mathrm{(electrons)} F} \ln{ \left( \prod\_i{ \left( A\_i^{\nu\_i} \right) } \right) }
\label{eq:emfDrGb}
\\]

Thus

\\[
\varepsilon = \varepsilon^{o} - \frac{R T}{\nu \mathrm{(electrons)} F} \ln{ \left( \prod\_i{\left( A\_i^{\nu\_i} \right) } \right) }
\label{eq:emfQ}
\\]

See [Balmer 2011](https://doi.org/10.1016/B978-0-12-374996-3.00004-X).

The cell potential can be calculated from the electrode potentials of the two balanced half-reactions ($\nu \mathrm{(electrons)}\_{cathode} = \nu \mathrm{(electrons)}\_{anode}$).

For a electrochemical cell with the cell formula

Anode (oxidation) $\ce{X –> z X+ + z e-}$ \| electrolyte \| $\ce{z Y+ + z e- -> Y}$ (reduction) Cathode

\\[
\varepsilon = \varepsilon\_{reduction, cathode} - \varepsilon\_{oxidation, anode}
\label{eq:emf}
\\]

See [here](https://chem.libretexts.org/Bookshelves/General_Chemistry/Book%3A_Chemistry_(OpenSTAX)/17%3A_Electrochemistry/17.4%3A_The_Nernst_Equation)

<br/>

Copyright 2020 DerAndere
