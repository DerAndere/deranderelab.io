---
layout: post
title:  Unbrick a BTT SKR Pro board
date:   2024-06-12 00:00:00 -0300
author: DerAndere
categories: tech
permalink: /unbrick-btt-skr-pro-board
tags: [tech, robots, open-source]
---


# How to fix the bootloader of a bricked BTT SKR Pro V1.1 board 

This article explains how to unbrick a BTT SKR Pro V1.1 board by BIGTREETECH that hangs and does not respond and that is not recognized even after reset (reboot, restart) when connected via USB. To install the bootloader, you have two options:


## Burning a bootloader on a BTT SKR Pro V1.1 board using a USB-to-Serial Adapter


A backup of the originl firmware, including the bootloader for the BTT SKR Pro V1.1 board, can be downloaded e.g. from [https://github.com/P-C-R/SKR_PRO_BOOTLOADER_REPAIR](https://github.com/P-C-R/SKR_PRO_BOOTLOADER_REPAIR).
Github user "c3-Dan" desrcibed a procedure for flahing of the bootloader using a USB to Serial Adapter with a FTDI USB to Serial bridge chip at [https://github.com/bigtreetech/BIGTREETECH-SKR-PRO-V1.1/issues/115#issuecomment-597939918](https://github.com/bigtreetech/BIGTREETECH-SKR-PRO-V1.1/issues/115#issuecomment-597939918):

1. Jumper SKR Pro 1.1 BOOT0 Pin to 3.3V
1. Get a FTDI Adapter
1. Download FLASHER-STM32 (STM32 Flash loader demonstrator UM0462): [https://www.st.com/en/development-tools/flasher-stm32.html](https://www.st.com/en/development-tools/flasher-stm32.html)
1. Make sure logic voltage switch on FTDI Adapter is 3.3v
1. Connect to SKR Pro 1.1 as follows : Connect FTDI 3.3v and ground to SKR 3.3v (near UART CONNECTOR) and ground pins
1. Connect FTDI TX to TFT Connector RX1 and FTDI RX to TFT Connector TX1 (For me the UART TX and RX Pins didnt work)
1. Open FLASHER-STM32, select the correct com port click next. If error appears, check wires
1. If OK, message will confirm connection Click Next again and and Next Again
1. In the next screen click the ... near the downoad to device section and select the .BIN file to upload.
1. Click next and wait.
1. When done, don't forget to remove the BOOT0 jumper.


## Burning a bootloader on a BTT SKR Pro V1.1 board using a STLink debugger and programmer

1. Buy a STLink debugger and programmer by STMicroelectronics [https://www.st.com/en/development-tools/stlink-v3minie.html](https://www.st.com/en/development-tools/stlink-v3minie.html)
1. Install STSW-LINK004 (STM32 ST-LINK utility): [https://www.st.com/en/development-tools/stsw-link004.html](https://www.st.com/en/development-tools/stsw-link004.html) 
1. Download the original firmware, including the bootloader for the BTT SKR Pro V1.1 board, e.g. from [https://github.com/P-C-R/SKR_PRO_BOOTLOADER_REPAIR](https://github.com/P-C-R/SKR_PRO_BOOTLOADER_REPAIR).
1. The rest of the procedure is described in a video by Peter Ross on youtube: [https://www.youtube.com/watch?v=7O_ZOdnM0SU](https://www.youtube.com/watch?v=7O_ZOdnM0SU)

<br/>

Copyright 2024 DerAndere