---
layout: post
title:  "Creating reports"
date:   2018-01-01 00:00:00 -0300
author: DerAndere
categories: data-analysis
permalink: /creating_repots
tags: [data-analysis, programming, Python, R]
---
Here you can find tips on management, processing and analysis of data.

# Creating reports

In order to get the most out of collected data, a good presentation of
the information is key. Several approaches for report generation are
available. If the information cannot be summarized in a single diagram
with a legend and a short description, it is worth to wisely choose the
software used for layout and type setting. One common approach is to use
LaTeX.

## Using LaTeX 

TeX is a typesetting system. Since version TeX82, it contains a turing
complete programming language (primitive TeX commands), as well as
"Plain TeX macros" for typesetting. Since version TeX 3, no new
features are added. LaTeX is a shortening for Lamport TeX. It provides a
program to process LaTeX documents (a compiler) and a macro package that
implements most plain TeX macros and adds many more TeX macros for easy
typesetting. The currently used version is LaTeX2e (LaTeX2ε). It is open
source software that is licensed under the terms of the LaTeX Project
Public License. Distributions such as TeX Live or MiKTeX provide an
installer that installs TeX (tex.exe), LaTeX2e (latex.exe), ConTeXt
(context.exe), LuaTeX (luatex.exe) and tools for maintainance and update
management like the MiKTeX console (miktex-console.exe and
miktex-console\_admin.exe).

### Software setup: MikTex, ghostscript, GSview, TexLipse and TexStudio, 

The following steps need to be followed:

1.  Make sure a PDF viewer is installed. The [freeware PDF-XChange Editor by Tracker Software](https://www.tracker-software.com/product/pdf-xchange-editor) is recommended.

2.  Install [ghostscript](https://www.ghostscript.com/)

3.  Install [GSView](https://gsview.com/) version 6 or newer by Artifex Inc.

4.  Install [MiKTeX](https://miktex.org/)

5.  Install [TexStudio](https://www.texstudio.org/).

6.  If something does not work, start TextStudio and left-click Options -\> Configure -\> TexStudio… -\> Commands. In the text field for the command Latex, specify the path to the executable file latex.exe within the MiKTeX installation directory (e.g. …MiKTeX 2.9/miktex/bin/x64/latex.exe). Proceed with setting up TexStudio as described in the [manual](http://texstudio.sourceforge.net/manual/current/usermanual_en.html). You may want to add MiKTeX 2.9/miktex/bin/x64/latex.exe as well as the executables for ghostscript and GSView to the PATH environment variable.

7.  \[Left-click Configuration. Activate "Advanced options" (option 3) at bottom left

8.  Leftclick Configuration -\> General. Uncheck (deactivate) "Logview with Tabs"

9.  Leftclick fast creation/fast compile. select Option 3 (pdflatex + Pdf Betrachter) \[or option 2 latex + dvi viewer\].

10. Leftclick Configuration -\> Editor: If your current project uses \\usepackage\[latin9\]{inputenc}, select font encoding ISO-8859-15. ?deactivate Auto detect? Font: Consolas, Font size: 11

11. Preview Command: dvipng – follow (parallel). Test with option "show inline". Then set to "only show in preview panel". Automatic preview: Select "Previously as preview translated text". Delay: 100\]

12. Install [Excel2LaTeX](https://ctan.org/pkg/excel2latex?lang=en)

13. Install [Zotero](https://www.zotero.org/download/)

14. Install and set up "[Better Bib(La)TeX for Zotero](https://retorque.re/zotero-better-bibtex/)"

15. Install [JabRef](http://www.jabref.org/)

16. Install [BibTex4Word](http://www.ee.ic.ac.uk/hp/staff/dmb/perl/index.html) by downloading the software and copying the file bibtex2word.dot from the downloaded folder into the directory C:\\Users\\\<Username\>\\AppData\\Roaming\\Microsoft\\Word\\STARTUP.

17. As an alternative to TexStudio, install the Eclipse plugin [TeXlipse](http://texlipse.sourceforge.net/).

### The preamble

Aaa

### Basics

Aaa

### Creating tables

aaa

### Inserting graphics

For plotting, use R with the packages ggplot2 and
[tikzDevice](https://cran.r-project.org/web/packages/tikzDevice/index.html)
or [gnuplot](http://www.gnuplot.info/).

### Creating custom BibTeX citation styles (style sheets)

In order to create a custom BibTeX bibliography style, follow the
following steps:

1.  Open C:\\Programme\\MiKTeX 2.6\\tex\\latex\\custom-bib

2.  Right-click makebst.text. In the context menu left-click "open with…" -\> "browse" -\> select C:\\Programme\\MiKTeX2.6\\miktex\\bin\\tex.exe

3.  Enter the input file: `merlin.msd`

4.  Enter output file: `<stylename>.bst`

5.  Answer last question of the dialog (batch process) with yes by entering y and pressing the enter-key.

6.  Search for the created file name (in the directory Windows/system32) and move the .dbj and .bst files to a convenient directory.

7.  Additionally, copy the .bst file to C:\\Programme\\MikTex2.6\\bibtex\\bst\\base

8.  Left-click Start -\> Programs -\> MiKTeX -\> Maintainance(Admin) -\> Settings (Admin) -\> Refresh FNDB

9.  Open your LaTeX editor (TeXstudio). You can now set the bibtex bibliography style for your LaTeX .tex files with the command `\\bibliographystyle{<stylename>}` in the preamble code of your LaTeX .tex files.

<br/>

Copyright 2018 DerAndere
