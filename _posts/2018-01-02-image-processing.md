---
layout: post
title:  "Image processing"
date:   2018-01-02 00:00:00 -0300
author: DerAndere
categories: data-analysis
permalink: /image-processing
tags: [data-analysis, programming, Python, R]
---

# Image processing and image analysis in Bio7 using ImageJ, Python and R

Bio7 is an open source program that is built on top of the Eclipse rich
client platform. Bio7 version 2.8 has a graphical user interface (R
perspective) for the editing of code written in the programming language
R and the [EclipseImageJ1Plugin (ImageJ2
version 1.52)](https://github.com/Bio7/EclipseImageJ1Plugin) tightly
integrated and simplifies the extraction of data from images, the data
manipulation in a spreadsheet view using the programming language R and
the conversion from data to image (plotting using R and imageJ) easy. It
also has the plug-in
[GEF](http://www.eclipse.org/gef/downloads/index.php)
integrated which provides a flow chart editor that is used for the
creation of documented and reproducible work flows and image/data
processing pipelines. After installation of Bio7 and Python3 to the
system partition (with the option "Add Python 3.x to PATH" during the
installation selected), on computers with Microsoft Windows as the
operating system, the environment variables might have to be edited to
add the folder containing the python.exe to the system Path. Additional
packages/libraries for Python provide easy access to important
functions. Most python packages can be downloaded and installed via the
command-line tool PIP that comes with the python3.x distribution for
Windows from [www.python.org](http://www.python.org). [Ensure that PIP
is properly installed. Edit the environment variables to add the folder
…/Python3x/Scripts that contains the executable file pip.exe to the
system
Path](https://matthewhorne.me/how-to-install-python-and-pip-on-windows-10/).
In order to download and install a python package that is available as
source archives or as a python wheel in the python package index, start
the command prompt (cmd.exe) and type the following into the command
prompt and confirm by pressing the \<enter\> key

`pip install <package>`

or:

`python -m pip install <package>`

On Windows you can use the python launcher to specify a version:

`py -3.7 -m pip install <package>`

Here, `<package>` is a placeholder that has to be replaced with the name
(without the `<>`) of the package on the python package index that you
want to install. For example, to install the package NumPy:

`py -m pip install numpy`

The following packages are recommended: NumPy,
[DatabaseInterfaces](https://wiki.python.org/moin/DatabaseInterfaces),
[mysqlclient](https://pypi.org/project/mysqlclient/), Cython, Py2exe,
Matplotlib, PyX, RPy, sh, Pillow, Pandas, pyserial, setuptools

Specify the path to to the executable file py.exe and python3X.exe. Then
open Bio7 and in the main menu of Bio7 click Help -\>install new
software or Help -\> Eclipse marketplace. Install the Eclipse plug-in
PyDev. Then, in the main menu of Bio7 click Preferences -\> preferences
-\> PyDev -\> Interpreters -\> Python Interpreter -\> New. Enter the
name to be displayed (e.g. "Python36") and specify the path to the
python36.exe. The Python interpreter menu has a tab "Packages", where
one can click on "Install/uninstall with PIP" in order to install new
packages from within Bio7.

## Image processing in Bio7 using the ImageJ perspective

In order to automate image processing, it is advisable to write imageJ
macros or Scripts. Possibly the easiest approach is to write macros
using the ImageJ macro language., create a New project (New -\> Project)
and give it some meaningful name. Add a new imageJ macro file to that
project (New -\> ImageJ -\> ImageJ macro file (.IJM)) and call it
appropriately. The final macro file has to be copied into the directory
…\\Bio7\\plugins\\com.eco.bio7.image\_x.xx.xxx\\macros. Finally,
inside Bio7, open the ImageJ Edit perspective and click on the toolbar
Plugins -\> Macros -\> Install and select the needed macro file. One
macro file can contain several macros. One can also edit the file
…\\Bio7\\plugins\\com.eco.bio7.image\_x.xx.xxx\\macros\\StartupMacros.txt
and add custom macros to that macro file. After installing that file,
those macros will be directly available after starting Bio7 from the
ImageJ Edit perspective toolbar Plugins -\> Macros without previously
reinstalling the file using Plugins -\> Macros -\> Install in the ImageJ
Edit perspective. I added the following code to the end of the file
…\\Bio7\\plugins\\com.eco.bio7.image\_x.xx.xxx\\macros\\StartupMacros.txt:

```
// @title: ImageProcessingByDerAndere
// @author: DerAndere
// @created: 2018
// Copyright 2018 DerAndere
// @license: MIT
// @version: 0.0.1
// @language: ImageJ macro language
// @info: https://it-by-derandere.blogspot.com
// @description: ImageJ macros for image processing.

macro "Batch-AdustBCsetMaxValue" {

// This macro processes all the images in a folder and any subfolders.

  extension = ".tif";
  extension2 = ".czi";
  inputDir = getDirectory("Choose Source Directory ");
  channel = getNumber("which channel should be adjusted?", 0);
  maxVal = getNumber("maxVal for the chosen channel?", 65535);

  //dir2 = getDirectory("Choose Destination Directory ");
  outputDir = inputDir + "BCSetMaxValueAdjusted\\" + "C" + channel +"\\";
  if(!File.exists(outputDir)) {
    File.makeDirectory(outputDir);
  }
  setBatchMode(true);
  n = 0;
  processFolder(inputDir);

  function processFolder(inputDir) {
     list = getFileList(inputDir);
     for (i=0; i<list.length; i++) {
          if (endsWith(list[i], "/")) {
              processFolder(inputDir+list[i]);
          }
          else if (endsWith(list[i], extension) || endsWith(list[i], extension2)) {
             processImage(inputDir, list[i]);
          }
      }
  }

  function processImage(inputDir, name) {
       img = inputDir + name;
  // only proceeds if file is there
    if(File.exists(img)){
      // opens the images and gives them the temporary name "imgTemp" 
      open(img);
      ID = getImageID(img);
      selectImage(ID);
      dotIndex = indexOf(name, ".");
      basename = substring(name, 0, dotIndex);
      rename("imgTemp");
      // the next lines do your procedure
      Stack.setChannel(channel);
      stackSize = nSlices(imgTemp); // or nSlices(); or nSlices; ?
      for(z=1; z<=stackSize; ++z){
        setSlice(z) //?
        run("setMinAndMax", "min=0 max=maxVal");
        run("Apply LUT");
        wait(500);
        selectImage(ID);
        wait(500);
      }
    titleMod = "C" + channel + "adj";
    titleTitle = basename + titleMod + ".tif";
    wait(500);
    // add code here to analyze or process the image
    saveAs("tiff", outputDir+titleTitle);
    wait(500);
    close();
    }
  }
}

macro "SaveCurrentSliceAsTIF" {
id = getImageID();
title = getTitle(); // or getTitle;?
dotIndex = indexOf(title, ".");
basename = substring(title, 0, dotIndex);
path = getDirectory("image");
outputDir = inputDir + "Slices\\";
if(!File.exists(outputDir)) {
  File.makeDirectory(outputDir);
}
currentSliceNumber = getSliceNumber();
tileTitle = basename + "_S" + currentSliceNumber + "_mod1.tif";
// using the ampersand allows spaces in the tileTitle to be handled correctly
run("Duplicate...", "title=&tileTitle");
selectWindow(tileTitle);
saveAs("tiff",outpurDir+tileTitle);
close();
selectImage(id);
}
// End of ImageProcessingByDerAndere
```

## Python scrips for image processing in the ImageJ perspective of Bio7 

aaa

## Transferring images into data using Bio7

aaa

## Manipulation and analysis of data using the R perspective of Bio7

In order to use R from within Bio7, left-click R -\> Preferences -\>
Preferences R. Select the correct Server. For Germany, try Germany
(Goettingen). Left-click R-shell, enter update.packages() to update
Rserve. If it is not installed, enter install.packages("Rserve").
Execute by pressing enter. After the successful installation is
confirmed in the R-shell, reboot your computer and restart Bio7 and
left-click R-shell. Then, left-click the tool "Start Rserve" from the
Bio7 toolbar at the top (or: R -\> Start Rserve). Open the R perspective
and Left-click "Packages" -\> Install packages and search for packages
you need. I recommend the packages dplyr and its dependencies that are
installed automatically with it (dbplyr, RMySQL, DBI, lubrify, knitr,
rmarkdown, ggplot2, RPostgreSQL, colorspace, utf8 and others),
[XLConnect](https://cran.rstudio.com/web/packages/XLConnect),
[xlsx](https://cran.rstudio.com/web/packages/xlsx),
[foreign](http://www.rdocumentation.org/packages/foreign),
[tidyr](http://blog.rstudio.org/2014/07/22/introducing-tidyr/).

## Alternatives to Image J: Darktable, Krita, Gimp 3.2 or Endrov

[Darktable is an excellent choice for radiometrically correct image
processing](https://ninedegreesbelow.com/photography/linux-raw-processor-review.html).
It allows you to completely deactivate any modules you don't want to
use, so you don't even have to look at them, and also allows you to
store presets for the remaining modules. darktable indicates the active
modules with an icon that can be clicked to disable the module in
question.

If you use Krita, open the image and left-click Left-click Image -\>
Properties… -\> Image color space. Change Model to greyscale/alpha, bit
depth (precision) 32bit floating point per channel. For 16 bit images in
a color space with a linear tone reproduction curve (gamma = 1.0),
select a Gray profile with linear tone reproduction curve (gamma = 1.0).
An example is the ICC profile "Gray-D50-elle-V4-g10.icc" (note: The
profile "Gray built-in (standard)" is the same as
"Gray-D50-elle-V2-g22.icc" and has a tone reproduction curve with
gamma = 2.2). Choose the rendering method "relative colorimetric",
activate "use black-point compensation" and left-click OK.. Image -\>
convert Image color space. Add a Layer on top of it and use it as an
[adjustment layer for non-destructive
editing](http://download.vikis.lt/doc/kde/HTML/en/krita/layers-adjustment.html)
by selecting that layer with a left-click in the layer overview to make
it the active layer and do level adjustments and/or inversion. By
default, the final aggregate will be in the same color space as the
bottom image. After level adjustments and inversions have been applied,
change back to 16bit integer precision and the profile "Gray (built-in)"
or better Gray-D50-elle-V2-srgbtrc.icc.

Gimp 3.2 (in development) will be fully color-managed.

[Endrov](http://www.endrov.net/wiki/index.php?title=Installing_EV) is an
alternative to ImageJ. Currently the contrast and brightness settings
are too limited, but an ImageJ plug-in is available.

[Icy](http://icy.bioimageanalysis.org/) includes ImageJ and Microscopy
Manager

## Deducer: a graphical user interface for the programming language R

Deducer is a specialized open source graphical user interface for data
analysis that is built around the JGR console for the programming
language R. In order to start Deducer from within the Bundled R
installation that comes with Bio7, you have to locate the path for your
Bio7 installation and open
.../Bio7\_x.x/plugins/Bundled\_R\_x.x.x/R/bin/x64 (all x stand for the
version numbers installed). In that folder, create a shortcut to the
file Rgui.exe and move this shortcut to a more convenient location. Open
the Rgui by double clicking the freshly created shortcut.
[Install the packages JGR, Deducer and
DeducerExtras,
DeducerText](http://www.deducer.org/pmwiki/pmwiki.php?n=Main.DownloadingAndInstallingDeducer).
Under Microsoft Windows this can be done by selecting from the Rgui menu
Packages -\> Install packages.... Select a CTRAN mirror from the list
and then select the packages JGR, Deducer and DeducerExtras. It is
recommendet to install the following additional packages:
DeducerSpatial, DeducerSurvival, DeduverText, ggplot2, Rcpp, RcppEigen,
Rserve, tm, utf8, XLConnect, XLConnectJars. Click OK and wait until all
packages are installed. 

To start Deducer using the Rgui console, select from the Rgui menu
Packages -\> Load package..., select JGR from the list and click OK.
Then type JGR() into the Rgui console and press \<enter\> to start the
JGR console. After all required packages were loaded, Deducer's Data
Viewer opens. By hovering the mouse over the main menu of the JGR
console (either File, Edit, Packages, Window or Help), the menu entries
Data, Plot and DeducerExtra are automatically added to the JGR console. 

In the JGR console, select from the menu Packages -\> Package manager
and choose the packages installed in the steps above to be loaded by
default with the JGR console and click refresh. Read the documentation
for R
([here](https://cran.r-project.org/doc/manuals/r-release/R-intro.html)
and
[here](https://cran.r-project.org/doc/manuals/r-release/R-lang.html#Introduction)),
for
[Deducer](http://www.deducer.org/pmwiki/pmwiki.php?n=Main.DeducerManual)
and for the packages mentioned above.

<br/>
Copyright 2018 - 2019 DerAndere
