---
layout: post
title: Project Management
date:   2020-09-14 00:00:00 -0300
author: DerAndere
categories: tech
permalink: /project-management/
tags: [tech, software-tips, open-source, data-analysis, data, science]
---

**[Werbung ohne Auftrag / unpaid advertisement]**

Project management tools that are designed with software development in mind, e.g. the open source tool [Ganttlab](https://gitlab.com/ganttlab/ganttlab), often have limited ability to display complex relationships between tasks (issues). On the other end of the spectrum, the open source tool ProjectLibre can export as comma-seperated value files (.csv), calculate resource use and costs, but lacks integration with other organizer tools (thunderbird, outlook, collaboration tools and version control tools).

## Inofficial Quick Guide for Redmine

For parallel management of complex and interdisciplinary projects, the open source software [Redmine](https://www.redmine.org/) is well suited. Redmine can be extended with hundreds of additional [plug-ins](https://www.redmine.org/plugins?page=1&sort=rating) such as the ["Redmine Outlook integration" by Ahau Software UNIP. LDA](https://www.redmine.org/plugins/redmine-outlook-addin) which can be [obtained for free](https://www.ahausoftware.com/redmine-outlook#prices). For calender export in .ics format, there is the plug-in [Mega Calendar](https://github.com/berti92/mega_calendar). For subscription to a webcal in ICS format, there is the plug-in [buschmais/redmics](https://github.com/buschmais/redmics). 

A personal computer on which Redmine is locally installed is called a "Redmine server". As long as the Redmine server is running the "Redmine Stack service", the Redmine projects can be accessed locally by authorized users or administrators by visiting `http://127.0.0.1` (localhost) in a web browser. Users can remotely access Redmine projects from another computer or smartphone (called "client device") only if the following conditions are met:

- The computer used as the Redmine server is running the "Redmine Stack service" (see section "Redmine setup" below)
- The user has a user account and valid user log-in credentials for the Redmine installation running locally on the Redmine server. Creation of a new Redmine user account is described in section "Redmine administration" below
- The firewall on the Redmine server allows remote access via the port that was opened by Redmine on the Redmine server (default port: 80)
- The client device and the Redmine server are conneced to the same (Wireless) Local Area Network
- The user runs a web browser on the client device and visits `http://<ip_address_of_the_redmine_server>`
- The user enters his log-in credentials

### Redmine setup

Setting up a Redmine server with a local installation of Redmine on a personal computer under Microsoft Windows involves the following steps:

1. Set a static IP adress for the personal computer that should be used as the Redmine server as described [here](https://portforward.com/networking/static-ip-windows-10.htm)
1. Install [Redmine using the installer from the self-contained Bitnami installations distribution locally on the personal computer that should be used as the Redmine server](https://bitnami.com/stack/redmine/installer).
1. If the Bitnami Redmine Stack is not launched automatically after the installation is completed, open the shortcut "Launch Bitnami Redmine Stack.html". You shoud get connected to `http://127.0.0.1:80/` (which means localhost: IP address 127.0.0.1, port 80) in a web browser.
1. Specify (and remember) your administrator login credentials (use pseudonyms but a real email address).
1. Click on "Access Redmine"
1. Sign out from Bitnami Redmine
1. Close the browser
1. Download the [master branch of the repository https://github.com/berti92/mega_calendar](https://github.com/berti92/mega_calendar/tree/master) as [a .zip archive](https://github.com/berti92/mega_calendar/archive/master.zip) and follow the [instructions to install redmine plug-ins for the Bitnami Redmine Stack](https://docs.bitnami.com/installer/apps/redmine/configuration/install-plugins/). [Installation of plug-ins for local installations of self-contained Bitnami distributions of Redmine under Microsoft Windows](https://www.redmineup.com/pages/help/installation/install-redmine-bitnami-package-and-plugins-on-windows#Plugins-installation) involves steps i) to viii):

    i. Download plugins and unpack them into the folder redmine/apps/redmine/htdocs/plugins . You may have to rename redmine/apps/redmine/htdocs/plugins/\<pluginname\>-master to redmine/apps/redmine/htdocs/plugins/\<pluginname\> (or, in the case of [buschmais/redmics](https://github.com/buschmais/redmics), rename the folder to "redmine/apps/redmine/htdocs/plugins/redmine_ics_export")

    ii. Go to the start menu and double-click on the shortcut "Use Bitnami Redmine Stack" which runs the script use_redmine.bat from the installation directory of Bitnami Redmine. The "Bitnami Redmine Environment" command line interface will be opened in a new console window.

    iii. Type the following command in the Bitnami Redmine Environment command line and confirm by pressing the enter key:
    ```
    cd apps\redmine\htdocs
    ```
    iv. In the Bitnami Redmine Environment command line, enter the following command:
    ```
    bundle install --without development test --no-deployment
    ```
    v. After completion of the installation, enter the following command in the same console:
    ```
    bundle exec rake redmine:plugins RAILS_ENV=production
    ```
    vi. Close the "Bitnami Redmine Environment" console window.

    vii. Stop the Bitnami Redmine Stack service (Go to the Start-menu and double-click on the shortcut "Stop Bitnami Redmine Stack service" which runs `servicerun.bat STOP` from the installation directory of Bitnami Redmine)

    viii. Start the Bitnami Redmine Stack service (Go to the Start-menu and double-click on the shortcut "Start Bitnami Redmine Stack service" which runs `servicerun.bat START` from, the installation directory of Bitnami Redmine)

1. Because you chose a self-contained installation (see step 2), follow the [instructions to set the sub-path for the plug-in Mega Calender](https://github.com/berti92/mega_calendar/wiki/Sub-Path) to `/redmine/`. See [here](https://docs.bitnami.com/bch/apps/redmine/get-started/understand-config/) for technical background.
1. If at some point in time nobody needs access to the projects on the Redmine projects, you can open the shortcut "Stop Bitnami Redmine Stack service" on the Redmine server. To enable access to the Redmine project, open the shortcut "Start Bitnami Redmine Stack service"

### Using Redmine

1. In order to use redmine, the Redmine server has to be connected to the (Wireless) Local Area Network and it must be running the Redmine Stack service. If in doubt, on the Redmine server start the Bitnami Redmine Stack service (Go to the Start-menu and click on the shortcut "Start Bitnami Redmine Stack service" to run `\Bitnami\redmine-4.1.1-4\servicerun.bat START`). 
1. Connect to the Redmine server by opening a web browser and visiting `http://<ip_address_of_the_redmine_server>` (for remote access from a client device) or `http://127.0.0.1:80`(for local access from the computer that is used as the Redmine server) 
1. Log in to Redmine as a user. Users with administrator privileges within Redmine have unlimited access to all Redmine features. Users without administrator privileges within Redmine do not have access to the tab "Administration" in the main menu of Redmine. 

The structure of the user interface is represented in the following diagram:
```
Redmine
├──Search
├──Home
├──MyPage
├──Calendar
├──Administration
│  ├──Users
│  │  ├──New user
│  │  ├──<Role name>
│  │  │  ├──Edit  
│  │  │  └──...
│  │  └──...
│  ├──Groups
│  │  ├──New group
│  │  ├──<Group name>
│  │  │  ├──Edit  
│  │  │  └──...
│  │  └──...
│  ├──Roles and permissions
│  │  ├──New role
│  │  ├──<Role name>
│  │  │  ├──Edit  
│  │  │  └──...
│  │  └──...
│  ├──Trackers
│  │  ├──New tracker
│  │  ├──<Tracker name>
│  │  │  ├──Edit  
│  │  │  └──...
│  │  └──...
│  └──...
├──Projects
│  ├──Create new project
│  ├──Issues
│  ├──Gantt
│  ├──Calendar
│  └──<Projectname>
│     ├──Overview
│     ├──Issues
│     │  ├──New issue
│     │  └──<Issue name>
│     │     ├──Edit
│     │     └──... 
│     ├──Gantt
│     ├──Calendar
│     ├──Documents
│     ├──Wiki
│     ├──Files
│     │  ├──New file
│     │  ├──<File name>     
│     │  │   ├──Edit
│     │  │   └──... 
│     │  └──...
│     └──Settings
│        ├──Project
│        ├──Members
│        ├──Issue categories
│        ├──Repositories
│        ├──Time tracking
│        └──...
├──Help
├──My Account
└──Sign out
```

#### Administration of a Redmine installation

It is advisable to create the following new roles ("Administration" -> "Roles and permissions" -> "New role"): "Researcher" (based on the default role "Developer") and "Assistant" (based on the default role "Reporter" but with "Issue visibility" set to "Issues created by or assigned to the user" and with permissions to "Edit own issues", "Set notes as private", "Edit own notes", "Set own issues as public or private", and "Manage sub-tasks").

Furthermore, it is advisable to create a new tracker ("Administration" -> "Trackers" -> "New tracker") with name "Task" (Set "Copy workflow from" to "Feature").

#### Task Planning with "Issues"

Tasks are represented as "Issues" within Redmine. To create new issues, go to "Projects" -> "Issues" -> "New issue". To view issues, go to the "Issue" list view, the "Gantt" diagram or the "Calendar". Left-click on the title (subject) of an issue opens the issue details and allows to add sub-tasks and further issue relations. Left-click on "Add" (next to the parameter "Related Issues") opens a drop-down menu for selection of the relation and a search field for selecting a related issue. The following relations are possible:

- Related to
- Blocked by
- Blocking
- Precedes
- Follows

Issues can have an optional parent issue. Due dates of parent issues are automatically recalculated based on the duration of sub-tasks. 

### Using the plug-in redmics: Subscribing to a Redmine webcal with an Email client like Thunderbird

1. Install [Mozilla Thunderbird](https://www.thunderbird.net/en-US/)
1. After installation of the Redmine plug-in [buschmais/redmics](https://github.com/buschmais/redmics) (see section "Redmine setup" above), log in to Redmine
1. In Redmine, go to "Projects" -> "Gantt". In the ICal sidebar, left-click on one of the links to get get a webcal address displayed in the address bar of the web browser.
1. Run Mozilla Thunderbird and add the desired Email account to Thunderbird.
1. In Thunderbird go to the calendar tab (main menu -> "Events and Tasks"). 
1. In the left side panel, left-click on the button with a plus symbol icon ("New calendar..."). Select source: "Network" -> "Next". Select format: iCalendar (ICS). In the address field, enter `webcal://<webcal-address\>` (replace the placeholder `<webcal-address>` with the webcal address from step 3). Left-click "Next". Enter a calendar name.

### Redmine Outlook integration

Ahau Software UNIP. LDA. provides the software tool ["Redmine Outlook integration"](https://www.redmine.org/plugins/redmine-outlook-addin) under a free license. The setup involves the following steps on the client computer:

1. On the client computer, close Microsoft Outlook
1. Obtain the software tool "Redmine Outlook integration" from Ahau Software UNIP. LDA. A [free license is available](https://www.ahausoftware.com/redmine-outlook#prices). 
1. Install "Redmine Outlook integration" on the client computer using the downloaded executable (.exe). 
1. Open Microsoft Outlook and go to the tab "Redmine" -> "Settings" -> "Redmine". Enter the Redmine URL. If the Redmine server runs a self-contained Bitnami Redmine installation, the Redmine-URL is 
```
http://<ip_address_of_the_redmine_server>/redmine/
```
Enter your Redmine user log-in credentials. In the same Settings dialog that is still open, click "Storage". [For installation on the computer that is used as the Redmine server, you may have to un-check (disable) the option "Use own Outlook Data File (.pst)"](https://ahausoftware.atlassian.net/wiki/spaces/WINKB/pages/462520321/Managing+Outlook+data+files+.pst+in+your+organization) to enable synchronization between Redmine and the default email account within Microsoft Outlook. In the same Settings dialog, click "Save". This will initiate the synchronization and adds a new, seperate Outlook Data File called "Redmine" to Microsoft Outlook. If the option "Use own Outlook Data File (.pst)" was disabled, the "Redmine" folder will instead be created within the default Email account. The following sub-folders are added: 

    - a new personal calendar called "Calendar", containing all assigned / created issues from Redmine
    - a new folder called "My Issues", containing a list of assigned / created issues 
    - a new folder called "Projects", containing a list of Redmine projects

Left-clicking  on an entry in the Calendar that was added to Microsoft Outlook by the Redmine Outook integration opens a Redmine panel where you can adjust parameters of the linked Redmine issue. 

## Tasks of a project manager

Project management has different meanings for different people. In the field of software development, a multitude of strategies and tools have emerged to streamline project management and consultants as well as service providers promote their own strategy, e.g. agile development (Scrum, Kanban, [Agile ASAP](https://blogs.sap.com/2013/09/17/the-all-new-asap-8-methodology/) or [Scale Agile Framework (SAFe)](https://www.scaledagileframework.com/). For scientists in academic research and for engineers, project management is more about compliance with laws and regulations. For example, chemists have to consider regulations for handling, transport and manufacturing of chemicals (Gefahrstoff-Verordnung, Betriebssicherheits-Verordnung in Germany) and biological material (Tierschutz-Versuchstier-Verordnung, Gentechnik-Sicherheits-Verordnung in Germany). For manufacturers in the chemical industry that produce drugs for medical applications, project management involves compliance with Good Manufacture Practice (GMP).

What follows is a typical list of tasks for a project manager in the pharmaceutical industry:

```
Clarify and update project goals with product manager or group leader 
Research literature
Establish documentation system
Keep documentation of project goals up to date
Ensure that available manuals, material safety data sheets and related documents are archived as part of the established documentation system
Document common business practices (Betriebsanweisung) according to TRGS 555 and document standard operating procedures (SOP)
Document risk assessment according to TRGS 400
Document power analysis (analyze test power)
Ask responsibles to install working space including safety measures
Get permission to use the laboratory from public authorities
In case of explosive chemicals:
    Create a "Explosionsschutzdokument" according to DGUV Information 213-106
In case of biological material: 
    Get "permission for experiments on animals" 
        Write applications for "permission for experiments on animals" 
        Ask responsibles to review applications for "permission for experiments on animals" 
    Get "permission for S2-S4 experiments" 
        Write applications for "permission for S2-S4 experiments" 
        Ask responsibles to review applications for "permission for S2-S4 experiments"
        Get "permission for S2-S4 experiments" from the public authorities  
    Announce further S2 experiments
        Write proposals for "announcements of further S2 experiments" 
        Ask responsibles to review proposals for "announcements of further S2 experiments" 
        Announce further S2 experiments to the public authorities
Ensure documentation of material according to GMP
Ensure documentation of chemicals
Ensure that materials get ordered
Ensure that chemicals get ordered
Ensure that chemicals get stored according to TRGS 510
Prepare scripts for data analysis
Prepare templates for reports
Ensure that required data gets collected according to the established documentation system
Report project status frequently

```

Additional information:

- [GMP](https://ec.europa.eu/health/documents/eudralex/vol-4_en)
- [AMWHV](https://www.gesetze-im-internet.de/amwhv/)
- [GefStoffV](http://www.gesetze-im-internet.de/gefstoffv_2010/BJNR164400010.html)
- [TRGS 510](https://www.baua.de/DE/Angebote/Rechtstexte-und-Technische-Regeln/Regelwerk/TRGS/TRGS-510.html)
- [TRGS 526](https://www.baua.de/DE/Angebote/Rechtstexte-und-Technische-Regeln/Regelwerk/TRGS/TRGS-526.html)
- [TRGS 555](https://www.baua.de/DE/Angebote/Rechtstexte-und-Technische-Regeln/Regelwerk/TRGS/TRGS-555.html)
- [TRGS 400](https://www.baua.de/DE/Angebote/Rechtstexte-und-Technische-Regeln/Regelwerk/TRGS/TRGS-400.html)
- [DGUV Information 213-850](https://downloadcenter.bgrci.de/shop/index.jsp?query=/bgi850-0.xml&field=path)
- [DGUV Information 213-106](https://publikationen.dguv.de/widgets/pdf/download/article/3360)
- [Cordes, Holzkamm 2007](https://his-he.de/fileadmin/user_upload/Publikationen/Forum_Hochschulentwicklung/fh-200709.pdf)
- [ZMT 2014](https://www.leibniz-zmt.de/images/zmt-staff/Dateien_f%C3%BCr_A-Z_am_ZMT/gb-labor.pdf)

<br>

Copyright 2020 DerAndere
