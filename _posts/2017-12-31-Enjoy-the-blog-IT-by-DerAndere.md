---
layout: post
title:  "Enjoy the blog  by DerAndere"
date:   2017-12-31 00:00:00 -0300
author: DerAndere
categories: DerAndere
---
-- Deutsche Version unten --

Hello world!

Welcome to the blog by DerAndere ([https://derandere.gitlab.io](https://derandere.gitlab.io)). I will write about  interesting news and events in and around the city of Bielefeld. You can follow my progress of learning everything about music (production), programming or digital photography. Most content will be written in English, the rest in German. Enjoy!

DerAndere

---------------------

Willkommen auf dem blog von DerAndere ([https://derandere.gitlab.io](https://derandere.gitlab.io)). Ich poste hier zu interessanten Neuigkeiten und zu Events in Bielefeld. Ihr könnt hier außerdem verfolgen, wie ich mich mit Themen wie Musik(produktion), Programmierung oder digitaler Photographie befasse und Wissenswertes dazu weitergebe. Vieles wird auf Englisch geschrieben sein, manches auch auf Deutsch.

Viel Spaß wünscht:

DerAndere 

<br/>

Copyright 2017 DerAndere
