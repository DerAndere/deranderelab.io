---
layout: post
title: Multi-axis-Marlin and Marlin2ForPipetBot
date:   2021-12-14 00:00:00 -0300
author: DerAndere
categories: tech
permalink: /Marlin2ForPipetBot
tags: [tech, robots, programming, open-source]
---

## Introduction

This article elaborates on my personal motivation for becoming a programmer and my experiences during the development of [Marlin2ForPipetBot](https://github.com/DerAndere1/Marlin), an extended version of Marlin firmware for CNC machines (3D printers, mills, laser cutters, hot wire foam cutters, lab robots).

## Background

The main motivation for me to learn programming was that I looked for a way to live out my creativity. I have been on a quest to tear down limitations on my creative freedom for many years and it started with music. I went from playing classical music to playing popular music on the piano, then rock music on the drums, then jazz on the drums and saxophone. Finally, creating electronic music allowed me to explore a whole new dimension of musical complexity and creativity. 

My quest continued with design: I still remember how I had loved to play with Lego bricks as a child. Later, I looked for something similar to Lego bricks that gave me more possibilities at lower costs. I was happy to find computer software that allowed me to build and animate 3D models from virtual Lego blocks. I then found much more freedom in general purpose computer aided design (CAD) software, so I learned how to use FreeCAD. 

For my job as a scientist, I was looking for open source image processing software. This is how I came in touch with GIMP, Krita and the open source community in general. I needed to understand how to analyze signal intensity in images in a linear manner and archive my results so that the results could be reproduced by any recipient. Along the way learned some basics about color science (ICC color profiles, Gamma / tone reproduction curves, color spaces) and reported bugs at the repositories for open source imaging software such as SCIFIO, ImageJ, GIMP and Scribus. At the same time, I thought that image processing and 3D modeling with graphical CAD software was quite cumbersome and time consuming. I had learned basics in the R programming language for data analysis and statistics at university, and in preparation for my thesis, I tought myself how to use LaTeX. 

I had not used my skills in real life and I could have easily lost my initial knowledge on the subject like most of my fellow-students did. But at some point I came to the conclusion that learning more about software development would give me the ultimative freedom. The idea that you could do whatever you wanted to do in the virtual world of software at no costs - besides time and efford ;) was intriguing to me. 

Thinking about how to start the learning process, I realized that physical programming of microcontrollers could give me a better understanding of how software interacts with computer hardware on a low level - something that has always been a fascinating puzzle for me. At the same time physical programming would give me a hands-on experience and immediate real world feedback. I decided that I wanted to use the Arduino framework because the many existing tutorials were easy to read and because hardware support ranged from very simple yet feature-complete 8 bit microcontrollers all the way up to 32bit boards with advanced peripherals (NodeMCU with its ESP32 or Arduino Due with ARM Core M3-based Microchip Atmel SAM3X8E). It was important for me to start with a microcontroller for which I could read and understand the complete datasheet at a reasonable time. 

I quickly read a lot of Arduino tutorials, but uploaded only a blinky sketch and a blink-without-delay sketch using the Arduino IDE. I read about better IDEs and chose Sloeber, which is the Arduino plugin for Eclipse IDEs. After thoroughly experimenting with the blinky-without-delay example sketch, I chose a MIDI conroller as my first noteworthy project. The tutorial I was following was well made and had just the right level of complexity. I used knowledge from other sources to modify the example code to suit my liking and made more use of modern C++ features. I read a lot about object oriented programming and encapsulation and about avoiding global variables. But having seen only simple source code using the Arduino framework, I had difficulties to understand its concepts. So I did a total rewrite of the MIDI controller code to strictly follow the object oriented programming paradigm. During all this, I took notes on my first blog. Then I turned my head at tools for collaboration and version control because this was interesting for me as a scientist who needs to document, revise and share infomation with collaborators. I had heared that git was the most modern approach, so I started using gitlab.com and Egit, the graphical user interface for the git client that is available as a plug-in for Eclipse IDEs. Around that time I moved my old blog to this homepage.

## The beginning of Multi-axis-Marlin

As my next project, I chose to build a lab robot. I did not want to spend too much tinkering time on the hardware, so I decided to convert a 3D printer. That gave me the frame, stepper motors and controller board to start with. The cheapest 3D printer kit which was supported by open source software was the Anet A8 by Shenzen Anet Inc. - The [PipetBot-A8](https://derandere.gitlab.io/pipetbot-a8/) project was born, and with it the Marlin2ForPipetBot firmware that was based on MarlinFirmware/Marlin. For version 1, I convert the extruder stepper motor into a fully fledged E-Axis. Therefore, I had to copy and adjust the code for the x axis endstop and homing and add it the code for the extruder stepper motor. I managed to get this working based on the bugfix-2.0.x branch, some months before the Marlin 2.0.0 tagged release. During that time, upstream Marlin firmware was changing heavily. After the Marlin 2.0.0 tagged release, I had considerable difficulties with rebasing my changes onto the updated Marlin code. 

After this was accomplished, I started to develop a graphical robot control software, deemed "Graphical G-code Generator" (GGCGen). For this poject I decided to learn how to use Python and wxpython. The reason was that Python seemed well suited also for data analysis, which I could use in science. At the same time it was fun and easy to use. wxpython provided a very solid cross-platform framework for GUI development that looks the same on all mayor operating systems. And it was much easier to use than the original wxwidgets library for C++. 

## Multi-axis support in Marlinfirmware/Marlin

When the basics were working, I decided to redo the Marlin2ForPipetBot firmware so that the pump was not driven by the E axis, but by an additional first-class axis. By choosing to add support for up to six non-extruder axes, I wanted to provide a more general solution. Adding support for up to six axes proved to be a valuable contribution to official MarlinFirmware/Marlin. From my experience with the E axis, I roughly knew which places I had to modify to get additional axes working. But simply copying code for the x axis did not yield a working solution. After several months, Gabriel Beraldo, who knew how to do hardware debugging, fixed an issue with the EEPROM code and contributed the first prototype with up to six axes that could be moved and auto-homed. 

A lot more bugs were fixed by other contributers and me in the following months. Rebasing onto updated Marlin bugfix-2.0.x proved to be very difficult due to some radical changes in MarlinFirmware/Marlin. Once again, I became more the maintainer and less an active developer. After some more months, contributor bilsef managed to find and fix the root cause for a bug that prevented movement of extra axes in a file called types.h and in the EEPROM. From that point onwards, I resumed my position as main developer of the Multi-axis support for Marlin. I managed to keep Multi-axis-Marlin up to date and was able to make more and more features compatible with more than 3 axes. Others contributed prototypes with preliminary  support for Trinamic TMC stepper drivers, sensorless homing, displays and so on. Those prototypes were sent to me as zip archives without version control. Also these prototypes were intended to be used only with a certain configuration, so I basically had to re-do most of the work.

Github user "hobiseven" repeatedly tested Multi-axis support and reported bugs. After everything was up to date and working, I decided to create a pull request targeting MarlinFirmware/Marlin bugfix-2.0.x . Scott "thinkyhead" Lahteine, the maintainer of MarlinFirmware/Marlin, did a lot of heavy refactoring and finally merged Multi-axis-support. It was satisfying to see how after two years our results were made accessible to a huge number of users. The advent of my multi-axis support in the most-used hobbyist 3D-printer firmware immediately sparked a lot of new projects. I was approached by a number of different scientists that set out to build special-purpose lab robots. There is now even a start up company that ships pick and place robots running Multi-axis-Marlin firmware. 

## Nine axes and beyond

In 2021, user "paloky" reported that he had a prototype with support for up to 10 axes (8 working) under version control. I was able to rebase his work onto my updated development branch. HendrikJan-5D was able to test a lot of the new features with his advanced 7 axis 3D printer setup. His bug reports led to further improvements. After more research, I decided that units and feedrate should be handled as it is done in LinuxCNC. I managed to implement the changes quickly and was able to release an updated version of Marlin2ForPipetBot (version 2.0.9.2) that supports up to 9 axes which can be either rotational or linear. It took quite a while to find out how to interpret feedrate for rotational moves. 
- [Fanuc and Siemens: when tool center point control (RTCP) is enabled (Fanuc G43.4) and there is no linear motion relative to the workpiece, the feedrate is interpreted in degrees per minute](https://www.emastercam.com/forums/topic/99385-problem-with-tcp-when-rotaries-are-singly-specified/?do=findComment&comment=1245881).
- LinuxCNC: According to documentation, feedrate is interpreted in degrees per minute if only rotational axes move. According to discussions in their discord channel, the last specified feedrate value is used and each F command overwrites the feedrate value that is used for any type of move.
- Haas: Feedrate is interpreted in linear units per minute. The feedrate is the linear feedrate along the circular trajectory defined by the [axis diameter (Setting 34)](https://www.haascnc.com/service/codes-settings.type=setting.machine=mill.value=S34.html). 

In 2022, Dan Royer ("i-make-robots") of Marginally Clever Robots, Ltd. revealed ["sixi3"](https://www.marginallyclever.com/products/sixi-3-robot-arm-diy-kit/), the first robot arm with six degrees of freedom (6 DOF) that uses Multi-axis-Marlin as its firmware. [Marginally Clever Robots also published their open source robot control host software "Robot-Overlord"](https://github.com/MarginallyClever/Robot-Overlord-App) (GPL-2.0 licensed).
In addition, [Dan Royer published a nice guide on how to modify Marlin for the robot](https://www.marginallyclever.com/2022/01/friday-facts-12-how-to-use-marlin-in-a-robot-arm/). The pre-configured firmware can be found in the [branch sixi3](https://github.com/MarginallyClever/Marlin-polargraph/tree/sixi3/). Kinematics are not implemented in the Robot-Overlord app that is used as host software to control the robot. [Dan Royer explains how to calculate kinematic parameters](https://www.marginallyclever.com/2022/01/friday-facts-13-how-to-find-dh-parameters-for-your-robot) and [how to configure the Robot-Overlord app to work with the robot](https://www.marginallyclever.com/2022/01/friday-facts-11-how-to-add-a-robot-arm-to-robot-overlord-2022/). 

In April 2022, my pull request for support of up to 9 rotational or linear axes was merged into Marlin bugfix-2.0.x. Since then I have been working on adding support for G43.4 rotary tool center point control (kown as TCP, RTCP or TCPC) for 5 axis CNC machines to Marlin. This required implementation of inverse kinematics.

The following video shows how I control a 4-Axis CNC machine prototype running Multi-axis-Marlin using my new G43.4 command (rotational tool center point control):

<video width="480" hight="360" controls preload="auto">
  <source src="/assets/DerAndere_XYZBC_HEAD_TABLE_G43_4_TCPC.mov" type="video/mp4" >
</video>

Video 1: Showcase of a 4-Axis CNC machine prototype running a [version of Multi-axis-Marlin with inverse kinematics for a 5 axis CNC machine with a swivel head and a horizontal rotary table (PENTA_AXIS_HT)](https://github.com/DerAndere1/Marlin/tree/Marlin2ForPipetBot). This firmware version offers the new tool center point control (TCPC) feature. The new G-codes G43.4 (enable TCPC) and G49 (disable TCPC) make continuous multi-axis operation of 3D printers easier. Copyright 2022 DerAndere. This video is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/. The .mov file can be downloaded [here](https://gitlab.com/DerAndere/derandere.gitlab.io/-/blob/master/assets/DerAndere_XYZBC_HEAD_TABLE_G43_4_TCPC.mov).

HendrikJan K prepared [a video on youtube](https://www.youtube.com/watch?v=XkLgc2DrPLg) showing a thorough test of [Multi-axis-Marlin with an early version of PENTA_AXIS_HT kinematics](https://github.com/DerAndere1/Marlin/tree/Marlin2ForPipetBot). The linked video is Copyright 2022 HendrikJan K. Licensed under the Creative Commons Attribution ShareAlike 4.0 International License (CC BY-SA 4.0). The following is the test G-code HendrikJan K used in his video:

```
M121
G49
G1 X0 Y0 Z21 F1500
G1 X-100
G1 X100
G1 X0
G1 Y-100
G1 Y100
G1 Y0
G1 B45
G1 B0
G1 C360
G1 C0
G43.4
G1 B45
G1 B0
G49
G1 X100
G43.4
G1 C360
G1 C0
G1 B45 C360
G1 B0 C0
G49
G1 X0 Y0
```

Most recently, I added support for 5 axis CNC machines with a tilting rotary table (PENTA_AXIS_TRT). The 3D printer of the [Open5x](https://github.com/FreddieHong19/Open5x) project represents this machine geometry.


<video width="480" hight="360" controls preload="auto">
  <source src="/assets/XYZBC_TRT_SKR-A8.mp4" type="video/mp4" >
</video>

Video 2: Demonstration of tool centerpoint control (TCPC) on the machine XYZBC_TRT_SKR-A8 by DerAndere. Copyright 2024 DerAndere. This video is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/). The .mp4 file can be downloaded [here](https://gitlab.com/DerAndere/derandere.gitlab.io/-/blob/master/assets/XYZBC_TRT_SKR-A8.mp4).


## CAM software for multi-axis machining

The biggest challenge when it comes to multi-axis machining is the CAM software that generates the G-code for the multi-axis maching center. Several approaches are currently explored by different people: 

- [https://doi.org/10.1145/3550454.3555516](https://doi.org/10.1145/3550454.3555516), [https://github.com/zhangty019/S3_DeformFDM](https://github.com/zhangty019/S3_DeformFDM)
- [https://doi.org/10.1145/3658212](https://doi.org/10.1145/3658212), [https://github.com/RyanTaoLiu/NeuralSlicer](https://github.com/RyanTaoLiu/NeuralSlicer)
- [https://github.com/epit3d/goosli](https://github.com/epit3d/goosli)
- [https://github.com/jyjblrd/Radial_Non_Planar_Slicer](https://github.com/jyjblrd/Radial_Non_Planar_Slicer)
- [https://github.com/marianocarpentier/gcode-modifier](https://github.com/marianocarpentier/gcode-modifier)
- [https://xyzdims.com/2021/02/08/3d-printing-penta-axis-pax-5-axis-printing-option/](https://xyzdims.com/2021/02/08/3d-printing-penta-axis-pax-5-axis-printing-option/)


## Where is this work used?

The following is a list of projects that are based on Multi-axis-Marlin:

- MarlinFirmware/Marlin: My 6-axis-Marlin merged with official [MarlinFirmware/Marlin](https://www.marlinfw.org), [https://github.com/MarlinFirmware/Marlin](https://github.com/MarlinFirmware/Marlin)
- [D. Vadivel, D.S. Branciforti, O. Kerroumi, M. Dondi, D. Dondi (2022). Mostly 3D printed chemical synthesis robot, HardwareX, 11, e00310.](https://doi.org/10.1016/j.ohx.2022.e00310)
- FOAMCUTTER_XYUV: My Marlin kinematics option for a 4 axis hot wire foam cutter, co-developed with Olivier Briand ("hobiseven") and rcKeith: [https://github.com/rcKeith/Marlin](https://github.com/rcKeith/Marlin).
- Marlin_9axis_OpenPnP by paloky: 6-axis-Marlin further extended to 9 axes for the OpenPnP project, an open source SMT pick-and-place robot: [https://github.com/paloky/Marlin_9axis_OpenPnP](https://github.com/paloky/Marlin_9axis_OpenPnP)
- lumenPnP by Stephen Hawes of opulo (previously index-machines): The commercialized open source SMT Pick-and-Place robot "index" by Stephen Hawes, based on Multi-axis-Marlin and OpenPnP: [https://www.opulo.io/products/lumenpnp](https://www.opulo.io/products/lumenpnp), [https://github.com/sphawes/index](https://github.com/sphawes/index), [https://github.com/opulo-inc/lumenpnp](https://github.com/opulo-inc/lumenpnp), [https://www.hackster.io/news/index-pnp-machine-can-control-up-to-30-component-feeders-at-the-same-time-20af2e651116](https://www.hackster.io/news/index-pnp-machine-can-control-up-to-30-component-feeders-at-the-same-time-20af2e651116) 
- Fork of Marlin2ForPipetBot for a fly monitoring robot by Daniele Dondi: [https://github.com/Daniele-Dondi/Marlin](https://github.com/Daniele-Dondi/Marlin)
- SAFE_BED_LEVELING_POSITION_X: My Marlin option for the 7 axis 3D printer by HendrikJan-5D from Hcc!3d [https://3d.hcc.nl/139-7-axis-5d-printer.html](https://3d.hcc.nl/139-7-axis-5d-printer.html)
- sixi3 by Dave Royer ("i-make-robots") of Marginallyclever Robots, Ltd.: A commercialized open source robot arm with six degrees of freedom (6 DOF) that runs Multi-axis-Marlin: [https://www.marginallyclever.com/products/sixi-3-robot-arm-diy-kit/](https://www.marginallyclever.com/products/sixi-3-robot-arm-diy-kit/), [https://github.com/MarginallyClever/Marlin-polargraph/tree/sixi3](https://github.com/MarginallyClever/Marlin-polargraph/tree/sixi3)
- 5D printer by HendrikJan K (HendrikJan-5D) of 3d.hcc.nl: [7 axis 3D printer by HendrikJan K (HendrikJan5D)](https://www.youtube.com/watch?v=_r7DA78GrXg)
- Candy Claw by Chris Riley of Chris's Basement: [Candy claw machine for ERRF 2022, running Marlin configured for 4 axes](https://github.com/brotherchris/Candy_Claw)
- openPnP-octopus by Murray Henwood: [Marlin firmware configured for another Pick-and-Place machine with BIGTREETECH Octopus board](https://github.com/murrayhenwood/Marlin-MultiAxis/tree/openPnP-octopus-dev)
- [Joshua M. MacEachern (2022). Theory, Design, and Development of an Open-Source 3D Printed Peristaltic Pump for Microfluidics Applications. Master Thesis. University of Alaska Fairbanks.](https://scholarworks.alaska.edu/bitstream/handle/11122/12940/MacEachern_J_2022.pdf), [https://www.reddit.com/r/functionalprint/comments/hsevpl/opensource_multichannel_microfluidicsosmm_pump/](https://www.reddit.com/r/functionalprint/comments/hsevpl/opensource_multichannel_microfluidicsosmm_pump/), [https://github.com/joshmaceachern/OSMMPump/blob/master/README.md](https://github.com/joshmaceachern/OSMMPump/blob/master/README.md).

<br/>

Copyright 2021 - 2024 DerAndere
