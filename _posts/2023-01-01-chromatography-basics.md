---
layout: post
title:  "chromatography basics"
date:   2023-01-01 00:00:00 -0300
author: DerAndere
categories: tech
permalink: /chromatography basics
tags: [tech] 
---

## mobile phase

### acidic

For RP-HPLC:
- A: 0.1% (v/v) acid in water
- B: 0.1% (v/v) acid in organic solvent

acid:
- normal: phosphoric acid
- compatible with mass spectrometry: formic acid
- for ion pair chromatography: tri-fluoro-acetic acid

organic solvent (decreasing polarity): 
methanol, isopropanol, acetonitrile, acetone, tetrahydrofuran

### buffered

For RP-HPLC:

With Methanol: 
- A: 5 to 20 mmol/L buffer in water
- B: 5 to 20 mmol/L buffer in a mixture of 90% (v/v) methanol, 10 % (v/v) water

For high buffer strength:
- A: 5 to 50 mmol/L buffer in water
- B: 5 to 50 mmol/L buffer in a mixture of 80% (v/v) methanol, 20 % (v/v) water

Otherwise:
- A: 5 to 20 mmol/L buffer in water
- B: 5 to 20 mmol/L buffer in a mixture of 80% (v/v) organic solvent, 20 % (v/v) water

At the isoelectric point (pI), compounds have a net charge of 0. With low purity column materials, it is important to use pH = pI(analyte) - 2 when analyzing acids or neutral compounds and pH = pI(analyte) + 2 when analyzing bases. This ensures a neutral analyte and thereby reduces strong Coulomb interactions with the unsubstituted silanol groups of the silica-based collumn matrix. 

Modern column matrix materials of high quality usually have a high purity (i.e. a high density of apolar moieties in the case of column matrices for reverse phase HPLC) and thus few unsubstituted silanol groups. With these columns, method development can be simplified. Use pH 2 to 4 if the sample is stable under those conditions.

Preferred buffers for reverse phase HPLC (RP-HPLC):

- Acetic acid - Ammonium acetate buffer, pH 2.7 or 3.0 
- Formic acid - Ammonium formate buffer, pH 3.8 or 4.0
- Phosphoric acid - Potassium phosphate buffer, pH 2.0

[Rob Beynon provides software that is capable of calculating exact buffer compositions](https://www.liverpool.ac.uk/pfg/Tools/BuffferCalc/Buffer.html)


### Choosing the solvent system

1. Default: Acetonitrile
2. For polar compounds: Methanol
3. For apolar compounds: Tetrahydrofuran

## Performing an analysis using HPLC

What follows is a list of links to resources that show how to perform analysis using HPLC with Chromeleon by Thermo Fischer Scientific:

- [Instrument control](https://www.youtube.com/watch?v=3kqyiXz110E)
- [Instrument method creation](https://www.youtube.com/watch?v=RReWg73hmmg)
- [Data processing](https://www.youtube.com/watch?v=EDm3CNw4Q0Q)
- [Method development and deactivation of metal surfaces in the flow path to increase inertness towards polar compounds](https://www.agilent.com/cs/library/technicaloverviews/public/5991-9271EN_HILIC_method_development_TechOverview.pdf)
- [HPLC system maintenance](https://www.agilent.com/cs/library/usermanuals/public/BestPractice_en.pdf)

<br/>

Copyright 2023 DerAndere