---
layout: post
title:  Using the PlatformIO IDE 
date:   2021-03-07 00:00:00 -0300
author: DerAndere
categories: programming
permalink: /platformio
tags: [programming, software-tips, open-source]
---

The following video tutorial explains how to install and use the PlatformIO IDE.

<video width="480" hight="360" controls preload="auto">
  <source src="/assets/PlatformIO1.mp4" type="video/mp4" >
</video>

Video 1: Embedded software development with the PlatformIO IDE. Copyright 2021 ITlernpfad. This video is dual-licensed under the terms of the [Apache License, version 2.0](https://apache.org/licenses/LICENSE-2.0.txt) or the [Creative Commons Attribution International 4.0 license (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/legalcode). This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/). The .mp4 file can be downloaded [here](https://gitlab.com/DerAndere/derandere.gitlab.io/-/blob/master/assets/PlatformIO1.mp4).

Have fun.

<br/>

Copyright 2021 DerAndere

