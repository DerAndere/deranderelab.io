---
layout: post
title: Software tips
date:   2020-08-29 00:00:00 -0300
author: DerAndere
categories: tech
permalink: /software-tips-overview/
tags: [tech, software-tips]
---

**[Werbung ohne Auftrag / unpaid advertisement]**

What follows is a carefully curated list of recommended free software (freeware or open source software). They are inteded for and experienced users and professionals in the sense that functionality has higher priority than ease of use. Software that only runs on GNU/Linux and requires expert knowledge for installation is excluded:

| Name            | Function        | Details | Links |
|-----------------|-----------------|---------|---------------------|
|openSUSE Leap KDE|Operating Sytstem|Can be installed next to Microsoft Windows (dual boot setup), up to date, stable, central system administration and software management (including drivers) with YaST2 (frontend for zypper)|[here](https://software.opensuse.org/distributions/leap/)|
|ghostscript      |Post script, EPS and PDF handling backend|Color managed|[here](https://www.ghostscript.com/download/gsdnld.html)|
|gsview           |Viewer for Post script and EPS|Frontent for ghostscript|[here](http://www.ghostgum.com.au/software/gsview.htm)|
|Inkscape         |SVG vector image editing|Trace bitmaps (vectorize), patterns, frontend for ghostscript, backend for Scribus 1.5.6, sRGB only. Needs Scribus 1.5.6|[here](https://inkscape.org/)|
|Scribus          |Desktop publishing|Color managed, frontend for Inkscape and ghostscript|[here](https://sourceforge.net/projects/scribus/files/scribus-svn/)|
|darktable        |RAW image processing, photo editing|Color managed|[here](https://derandere.gitlab.io/image-processing)|
|Fiji             |(batch-)processing of scientific images, image analysis|Import of scientific raw image formats with SCIFIO, sRGB or raw formats only. Needs [colorTransformer2](http://www.russellcottrell.com/photo/colorTransformer2.htm) and [ij-plugins_toolkit](https://sourceforge.net/projects/ij-plugins/files/ij-plugins_toolkit/)|[here](https://derandere.gitlab.io/image-processing)|
|Python 3 + pillow|Batch processing of images, scripted image analysis, scripted image editing, color management|Library for Python 3, reliable, flexible|[here](https://derandere.gitlab.io/image-processing)|
|Python 3 + wxpython + subprocess|Developing programs that have a GUI |Library for Python 3, platform-independent, easy, flexible|[here](https://www.wxpython.org/)|
|Python 3 + SQLAlchemy + pymysql|Manipulating data in databases |Library for Python 3, platform-independent, database-agnostic, flexible|[here](https://derandere.gitlab.io/databases)|
|Python 3 + numpy + pandas + scipy + statsmodels + matplotlib |statistical analysis, test power analysis|Library for Python 3|[here](https://towardsdatascience.com/introduction-to-power-analysis-in-python-e7b748dfa26)|
|Python 3 + numpy + pandas + scipy + sympy + matplotlib |Computer Aided Algebra|Library for Python 3, solve equations and differntials analytically (symbolic algebra, symbolic calculus)|[here](https://towardsdatascience.com/introduction-to-power-analysis-in-python-e7b748dfa26)|
|LibreOffice      |Office suite|Writer: text editing, PDF support, Calc: spreadsheets, Draw: presentations. Better than Microsoft Office|[here](https://derandere.gitlab.io/creating_repots)|  
|TexStudio        |Latex Editor, typesetting|live preview, frontent for MikTex|[here](https://derandere.gitlab.io/creating_repots)|  
|MikTex           |Latex distribution, typesetting |Runs on Microsoft Windows|[here](https://derandere.gitlab.io/creating_repots)|  
|Zotero standalone|Literature management, referencing system |platform-independent, LibreOffice integration|[here](https://derandere.gitlab.io/creating_repots)|  
|JabRef           |Literature management, referencing system |platform-independent, LibreOffice integration|[here](https://derandere.gitlab.io/creating_repots)|  
|Thunderbird      |Organizer |Email, Calendar, Contacts. Emails can get lost when copied from IMAP inbox|[here](https://www.thunderbird.net/en-US/)|
|K-9 Mail         |Organizer |Apache License, Verions 2.0. Email client apk for android smartphones and iPhones running iOS|[here](https://github.com/k9mail/k-9)|
|Bitnami Redmine + buschmais/redmics plug-in + Redmine Outlook integration|Project management|GNU General Public License 2.0|[here](https://derandere.gitlab.io/project-management/)|
|FreeCAD + Assembly4|Computer aided design |GNU General Public License 3.0, Assembly 4 extension can be installed|[here](https://github.com/FreeCAD/FreeCAD/releases/tag/)|
|Zyn-Fusion       |Synthesizer VST plug-in |ZynAddSubFX with graphical user interface "Fusion" (uses the zest framework)|[here](https://derandere.gitlab.io/free-synth-vst-plug-ins/)|
|Chameleon        |Synthesizer VST plug-in |Amplifier simulator for clean and distorted (overdrive) sounds |[here](https://guitarml.com/#products)|
|MusicBeam        |Light show |MIT License |[here](https://www.musicbeam.org/)|
|Captura          |Screen recording |Only for Microsoft Windows, Highlight mouse cursor and show key strokes|[here](https://mathewsachin.github.io/Captura/)|
|vokoscreenNG     |Screen recording |Highlight mouse cursor and show key strokes|[here](https://github.com/vkohaupt/vokoscreenNG)|
|kdenlive         |Video editing |GNU General Public License 2.0|[here](https://invent.kde.org/multimedia/kdenlive)|
|shotcut          |Video editing |GNU General Public License 2.0|[here](https://shotcut.org/download/)|
|VSCode + PlatformIO IDE |Programming |Apache License, Version 2.0|[here](https://platformio.org/platformio-ide)|
|Bio7             |Integrated development environment for software develpment |Based on the Eclipse framework, integration of ImageJ and R, Eclipse IDE packages can be installed|[here](https://derandere.gitlab.io/programming-overview)  ; [here](https://www.eclipse.org/jdt/core/update-site/)|  
|ChemAxon Marvin  |Chemistry |Free for non-commercial use|[here](https://chemaxon.com/products/marvin)|
|KNIME            |Chemistry, Data science, Programming|GNU General Public License 3.0, Infocom/Axonchem KNIME nodes (Marvin nodes), RDKit KNIME nodes for chemistry, Eclipse IDE packages can be installed|[here](https://www.knime.com/downloads/download-knime) ; [here](https://github.com/knime/knime-sdk-setup)  ; [here](https://www.eclipse.org/jdt/core/update-site/)|
|Bioclipse        |Chemistry, Programming |Based on Open Babel, Chemistry Developemt Kit, ChemPaint and the Eclipe framework. Eclipse IDE packages can be installed|[here](https://souceforge.net/projects/bioclipse/) ; [here](https://www.eclipse.org/jdt/core/update-site/)|
|SerialCloner     |Molecular biology  |Freeware|[here](http://serialbasics.free.fr/Serial_Cloner-Download.html)|
|GeneRunner|Molecular biology |Freeware ; only for Microsoft Windows|[here](http://www.generunner.net)|
|BLAST (primer-blast, protein blast)|Molecular biology |web-interface online|[here](https://blast.ncbi.nlm.nih.gov/Blast.cgi) ; [here](https://www.ncbi.nlm.nih.gov/tools/primer-blast/)|
|protparam        |Molecular biology |web-interface online|[here](https://web.expasy.org/protparam/) ; [here](https://www.uniprot.org/)|
|PubMed           |Scientific literature |web-interface online|[here](https://pubmed.ncbi.nlm.nih.gov/)|
|PubChem          |Chemistry literature |web-interface online|[here](https://pubchem.ncbi.nlm.nih.gov/)|
|NIST             |Chemical data |web-interface online|[here](https://webbook.nist.gov/chemistry/) ; [here](https://www.nist.gov/pml/productsservices/physical-reference-data)|
|NIST Digital Library of Mathematical Functions|Database containing mathematical functions|web-interface online|[here](https://dlmf.nist.gov/)|
|EAGLE 7.7.0      |Computer aided design |"Express" licensing: Free for non-commercial use|[here](https://eagle.autodesk.com/eagle/software-versions/1)|
|KiCAD            |Computer aided design |GNU General Public License 3.0. Electronic design automation software for creation of schematic circuit diagrams and designing layouts for printed circuit boards (PCB)|[here](https://www.kicad.org/download/)|
{:.mbtablestyle}

<br>

Copyright 2020 - 2022 DerAndere
