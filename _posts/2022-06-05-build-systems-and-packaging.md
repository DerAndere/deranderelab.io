---
layout: post
title:  "Software Packaging and Build Systems"
date:   2022-06-04 00:00:00 -0300
author: DerAndere
categories: programming
permalink: /packaging
tags: [programming, Python] 
---

## Software Build Systems, Packaging and Distribution

Software development includes not only software design, programming, testing and debugging, it also extends to packaging and distribution.

The build process and other tasks differ, depending on the programming language used.

#### Python (e.g. [GGCGen](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen)):


| Step                   | recommended tool                         | alternative tools    |
|------------------------|------------------------------------------|----------------------|
| build system           | none (CPython interpreter)               | py2exe, cyton        |
| compilation toolchain: | interpreted (CPython interpreter)        | py2exe, cyton        |
| Packaging              | PyPa build plus setuptools with setup.cfg, setup.py and MANIFEST.in | d2to1, distutils2 |
| Distribution           | PyPa, pip                                | git repository + pip |
{:.mbtablestyle}

#### C/C++ (e.g. [tutaconvert](https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert) and [direction_indicator](https://gitlab.com/itlernpfad_public/direction_indicator)):

| Step                   | recommended tool                         | alternative tools    |
|------------------------|------------------------------------------|----------------------|
| build system           | gitlab-CI with _config.yml, Microsoft Visual Studio code + PlatformIO IDE + MSYS2 with base-devel option (git, make, cmake, gcc, g++) ) |  Microsoft Visual Studio code + CMake tools + MSYS2 with base-devel option, travis-CI |
| compilation toolchain: | PlatformIO platform avr (avr-toolchain with avr-gcc) | GCC      |
| Packaging              | CMake, CPack, NSIS                       | PlatformIO           |
| Distribution           | ?                                        | ?                    |
{:.mbtablestyle}

<br/>

Copyright 2022 DerAndere