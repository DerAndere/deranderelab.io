---
layout: post
title:  "wxWidgets and Visual Studio Code"
date:   2024-12-13 00:00:00 -0300
author: DerAndere
categories: programming
permalink: /wxwidgets
tags: [programming, C++] 
---

## wxWidgets and Visual Studio Code

wxWidgets is a cross-platform C++ library for the creation of software with a graphical user interface (GUI).

To use it from within Visual Studio Code (VSC), follow these steps:
1. Install Git
1. Install Visual Studio Code
1. Install C++ Extension and Sorce control extension for VSC
1. Install MSYS2
1. update MSYS2 using pacman from the MSYS shell
1. install and update the base-devel package of MSYS2 using pacman from the MSYS shell
1. Edit the system environment variable PATH to add the following paths: 

```
D:\Git\bin
D:\msys64\mingw64\bin
D:\msys64\usr\bin
```


Although MSYS2 provides precompiled packages for wxWidgets, you can compile it from source as follows:


1. Download the source code for wxWidgets and extract it to a path without spaces or special characters, e.g. to D:\wxWidgets326
1. Add a system environment variable with name WXWIN and set the value to match the path of the directory in which the source code for wxWidgets resides, e.g D:\wxWidgets326. 
1. Run cmd.exe or open the terminal view in VSC and change directory (cd) to %WXWIN. 
1. Then, for a static 64bit library enter the following commands, see [https://github.com/PBfordev/wxpbguide/](https://github.com/PBfordev/wxpbguide/) and [https://forums.wxwidgets.org/viewtopic.php?p=206033#p206033](https://forums.wxwidgets.org/viewtopic.php?p=206033#p206033) and [https://wiki.wxwidgets.org/Compiling_wxWidgets_with_MinGW](https://wiki.wxwidgets.org/Compiling_wxWidgets_with_MinGW):

```
cd %WXWIN\build\msw

mingw32-make -f makefile.gcc SHARED=0 BUILD=debug SHELL=cmd.exe
```

1. And for a release build:

```
cd %WXWIN\build\msw

mingw32-make -f makefile.gcc SHARED=0 CFG=_x64 BUILD=release SHELL=cmd.exe
```

git clone https://github.com/huckor/wxwidgets-vscode and follow the instructions in its README file.

other resources:
https://github.com/ThOsbo/wxWidgetTemplate
https://github.com/theprogrammer67/wxwidgets-vscode

For cmake (the easieast solution, works from MSYS2 MSYS shell:)
https://www.youtube.com/watch?v=tHMGA0jIl3Y
https://github.com/tontosirikul/wxWidgets-vscode-template

https://github.com/eranif/wx-config-msys2

https://docs.wxwidgets.org/latest/overview_cmake.html

```
export PATH=/mingw64/bin:$PATH

cd %WXWIN

mkdir build-debug

cd build-debug

cmake .. -G"MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug -DwxBUILD_SHARED=OFF

mingw32-make

```

the static libraries will be in D:\wxWidgets326\lib\gcc_lib

starting with https://github.com/huckor/wxwidgets-vscode, modify

.vscode/c_cpp_properties.json. For the release build:

```
        {
            "name": "Win32",
            "includePath": [
                "${workspaceFolder}/src/**",
                "${workspaceFolder}/dep/win/lib/gcc_lib/mswu",
                "${workspaceFolder}/dep/win/include"
            ],
            "defines": [
                "_WINDOWS",
                "_UNICODE",
                "__WXMSW__",
                "NDEBUG",
                "NOPCH"
            ],
            "compilerPath": "c:\\mingw64\\mingw64\\bin\\g++.exe",
            "cStandard": "c11",
            "cppStandard": "gnu++14",
            "intelliSenseMode": "gcc-x86"
        },

```

to


```
        {
            "name": "Win32",
            "includePath": [
                "${workspaceFolder}/src/**",
                "${env:WXWIN}\\lib\\gcc_lib_x64\\mswu",
                "${env:WXWIN}/include"
            ],
            "defines": [
                "_WINDOWS",
                "_UNICODE",
                "__WXMSW__",
                "NDEBUG",
                "NOPCH"
            ],
            "compilerPath": "D:\\msys64\\mingw64\\bin\\g++.exe",
            "cStandard": "c11",
            "cppStandard": "gnu++14",
            "intelliSenseMode": "gcc-x86"
        },


            "name": "Win32",
            "includePath": [
                "${workspaceFolder}/src/**",
                "${env:WXWIN}/lib/gcc_lib/mswu",
                "${env:WXWIN}/include"
            ],

```

and edit file tasks.json




<br/>

Copyright 2024 DerAndere