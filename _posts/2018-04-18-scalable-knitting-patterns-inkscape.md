---
layout: post
title:  Scalable knitting patterns with open source software - Textile design with Inkscape and GIMP
date:   2018-04-18 00:00:00 -0300
author: DerAndere
categories: tech
permalink: /scalable-knitting-patterns-inkscape
tags: [tech, software-tips, open-source]
---

I made a video tutorial that explains how to design knitting patterns that can be scaled to arbitrary size using the open source programs Inkscape v0.92 and GIMP 2.10.10. It may be especially interesting to the community of the All Yarns are Beautiful (AYAB) project:

<video width="480" hight="360" controls preload="none">
  <source src="/assets/pattern_creation_inkscape.mp4" type="video/mp4" >
</video>

Video 1: Scalable knitting patterns with open source software. Copyright 2019 DerAndere. This video is licensed under the terms of the [Creative Commons Attribution International License 4.0 (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/legalcode) or the [Creative Commons Attribution ShareAlike International 4.0 license (CC BY SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/legalcode). The .mp4 file can be downloaded [here](https://gitlab.com/DerAndere/derandere.gitlab.io/-/blob/master/assets/pattern_creation_inkscape.mp4).

Have fun exploring the possibilities! Do you have suggestions how the tutorial can be improved? Please give feedback via [Github](https://github.com/DerAndere1/ayab-manual/issues) or in the [facebook group "AYAB All Yarns are Beautiful"](https://de-de.facebook.com/groups/1384431355220966). Or do it like I did and contribute to the official [ayab-manual repository](https://github.com/AllYarnsAreBeautiful/ayab-manual).

<br/>

Copyright 2018 DerAndere
