---
layout: page
title: License
permalink: /license/
---

Licensing Information User Manual

DerAndereOnline ([https://derandere.gitlab.io](https://derandere.gitlab.io))

     __________________________________________________________________

## Introduction

   This License Information User Manual contains the license
   granted by DerAndere.

   Last updated: March 2022.

## Licensing Information

   Copyright 2017 - 2022 DerAndere

   Unless otherwise noted, content at [https://derandere.gitlab.io/products](https://derandere.gitlab.io/products) is 

   Copyright 2017 - 2022 DerAndere. All rights reserved.

   Unless otherwise noted, content at [https://derandere.gitlab.io](https://derandere.gitlab.io) except [https://derandere.gitlab.io/products](https://derandere.gitlab.io/products)
   is licensed under the Creative Commons Attribution 4.0 Interntional License. 
   To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ OR licensed 
   under the terms of the MIT License.

### MIT License

Copyright 2017 - 2022 DerAndere

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
