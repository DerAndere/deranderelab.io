---
layout: page
title: Our Products
permalink: /products/
---

# Support us
Your donations help us to make even better products:

[Donate to DERANDERE](https://www.paypal.com/donate/?hosted_button_id=TNGG65GVA9UHE)

### TutaConvert
A tool to convert exported contacts from tutanota into a valid vCard file for migration of address books to other organizer software / Email clients like Thunderbird.

[License](https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/-/blob/main/LICENSE)

[Download TutaConvert installer for Microsoft Windows 64 bit](https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/uploads/bc1716a4f201a0d7f7af17146bece139/tutaconvert-0.1.0-win64.exe)

### GGCGen
Graphical G-code Generator and robot control software.
This program provides a graphical user interface (GUI) for easy 
creation of methods (scripts) for controlling cartesian robots - also known as 
lab robots, liquid handling robots or pipetting robots. GGCGen provides a 
variety of predefined working space layouts and customizable method blocks for 
point-and-click creation of (partially opentrons API v2-compatible) 
protocols that are executed by the robot.

[License](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/-/blob/master/LICENSE)

[Download GGCGen Python wheel](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/uploads/392afab9c6f93983759a18165e98f21f/GGCGen-1.0.7-py3-none-any.whl)

### Marlin2ForPipetBot
Optimized firmware for cartesian robots (lab robots, also known as liquid handling robots or pipetting robots).

[License](https://github.com/DerAndere1/Marlin/blob/Marlin2ForPipetBot/LICENSE)

[Download Marlin2ForPipetBot](https://github.com/DerAndere1/Marlin/archive/refs/tags/Marlin2ForPipetBot-2.0.9.2.zip)

### pyotronext
Python library for lab robot control. pyotronext is an open source Python package for
conversion of opentrons APIv2
or mecode compatible Python protocols into
a dialect of the ISO 6983-1 and ANSI/EIA RS274-D G-code is compatible with open source firmware for CNC machines.
pyotronext can control liquid handling robots via USB using the 8-N-1 asynchronous serial communication protocol
(8 data bits, no parity, 1 stop bit).

[License](https://gitlab.com/RobotsByDerAndere/pyotronext/-/blob/master/LICENSE)

[Download pyotronext Python wheel](https://gitlab.com/RobotsByDerAndere/pyotronext/uploads/01c52c3bf9281b1299ff6f084a1b32a4/pyotronext-1.0.7-py3-none-any.whl)
